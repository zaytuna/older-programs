import java.util.ArrayList;

class PossibleAnswers
{
	ArrayList<String> jkl = new ArrayList<String>();

	PossibleAnswers(int b)
	{
		if(b>0)
		{
			for(int i = 0; i < b; i++)
				jkl.add("");
		}
	}
	public void setValue(int index, String value)
	{
		try
		{
			if(index<0)
				throw new IndexOutOfBoundsException();
			jkl.set(index,value);
		}
		catch(IndexOutOfBoundsException ioobe)
		{
			jkl.add(value);
		}
	}
	public void setValues(String... hi)
	{
		jkl.clear();

		for(int i = 0; i < hi.length; i++)
		{
			jkl.add(hi[i]);
		}
	}
	public void ensureFor(int n)
	{
		for(int i = jkl.size(); i <n; i++)
			jkl.add("");
	}
	public void addValue(String value)
	{
		jkl.add(value);
	}
	public String toString()
	{
		String st = "";
		for(String j : jkl)
			st+=j+",\t";
		return st;
	}
	public ArrayList<String> getValues()
	{
		return jkl;
	}
	public String getValueAt(int in)
	{
		try
		{
			return jkl.get(in);
		}
		catch(IndexOutOfBoundsException e)
		{
			e.printStackTrace();
			System.out.println("INDEX: "+in+"\tjkl.length: "+jkl.size());
		}
		return null;
	}
}