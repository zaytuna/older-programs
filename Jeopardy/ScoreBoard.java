import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

class ScoreBoard extends Colorful implements ActionListener
{
	public ArrayList<Player> players = new ArrayList<Player>();
	private JButton newPlayer = new JButton("New Player");
	JLabel turnL = new JLabel("", JLabel.CENTER);
	JeopardyDriver owner;

	ScoreBoard(JeopardyDriver cj)
	{
		super(Color.GREEN,Color.BLACK,new Point(100,30));
		setPreferredSize(new Dimension(180,600));
		setMinimumSize(new Dimension(180,600));
		setLayout(null);
		arrangeComponents();
		turnL.setFont(new Font("SERIF",Font.PLAIN,22));
		newPlayer.addActionListener(this);
		this.owner = cj;
		update();
	}
	public void arrangeComponents()
	{
		int row = 0;
		while(row<players.size())
		{
			players.get(row).representation.setBounds(5,row*80+23,155,65);
			add(players.get(row).representation);
			row++;
		}

		newPlayer.setBounds(5,row*80+23,155,30);
		turnL.setBounds(3,row*80+48,166,35);
		add(newPlayer);
		add(turnL);
		setPreferredSize(new Dimension(180,row*80+90));
	}
	public void paint(Graphics g)
	{
		super.paint(g);
		try
		{
			Graphics2D g2d = (Graphics2D)g;
			g2d.setStroke(new BasicStroke(3,BasicStroke.CAP_ROUND,BasicStroke.JOIN_MITER));
			g2d.setColor(players.get(owner.turn).color);

			g2d.drawLine(175,players.get(owner.turn).representation.getY()+32,175,players.size()*80+62);//draw Vertical line
			g2d.drawLine(175,players.get(owner.turn).representation.getY()+32,165,players.get(owner.turn).representation.getY()+32);
			//Draw Arrows in the next two lines
			g2d.drawLine(165,players.get(owner.turn).representation.getY()+32,170,players.get(owner.turn).representation.getY()+41);
			g2d.drawLine(165,players.get(owner.turn).representation.getY()+32,170,players.get(owner.turn).representation.getY()+23);

			g2d.drawLine(175,players.size()*80+62,152,players.size()*80+62);//drawHorizontal line
		}
		catch(IndexOutOfBoundsException e)
		{
		}
	}
	public void actionPerformed(ActionEvent evt)
	{
		if(evt.getSource() == newPlayer)
		{
			players.add(new Player(this));
			arrangeComponents();
			owner.repaint();
		}
	}
	public void update()
	{
		try
		{
			for(Player p : players)
			{
				p.representation.updateInfo();
			}
			turnL.setForeground(players.get(owner.turn).color);
			turnL.setText(players.get(owner.turn).name+"'s turn");
			repaint();
		}
		catch(IndexOutOfBoundsException npe)
		{

		}
	}
	public void setPoints(int index, int points)
	{
		players.get(index).points = points;
		update();
	}
}