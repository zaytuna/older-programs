import java.util.*;

class CatagoryList extends ArrayList<Catagory>
{
	private int numOfPossibleAnswers = 3;
	int step = 100;
	public int largestLength()
	{
		int length = 0;
		for(Catagory c:this)
		{
			if(c.questions.size()>length)
				length = c.questions.size();
		}
		return length;
	}
	public void addToAll(String q, String a, int pos)
	{
		for(Catagory c:this)
		{
			if(pos>=0)
			{
				c.questions.add(pos,q);
				c.answers.add(pos,a);
				PossibleAnswers pa = new PossibleAnswers(numOfPossibleAnswers);
				String[] strs = new String[numOfPossibleAnswers];
				for(int i = 0; i < strs.length; i++)
					strs[i] = "";
				pa.setValues(strs);
				c.poss.add(pos,pa);
			}
			else
			{
				c.questions.add(q);
				c.answers.add(a);
				PossibleAnswers pa = new PossibleAnswers(numOfPossibleAnswers);
				String[] strs = new String[numOfPossibleAnswers];
				for(int i = 0; i < strs.length; i++)
					strs[i] = "";
				pa.setValues(strs);
				c.poss.add(pa);
			}
		}
	}
	public String toString()
	{
		String st = "";
		for(int i = 0; i < largestLength(); i++)
		{
			for(int j = 0; j<size(); j++)
			{
				st += get(j).questions.get(i)+":"+get(j).answers.get(i)+"\t";
			}
			st+="\n";
		}
		return st;
	}
	public int getNumOfPossibleAnswers()
	{
		return numOfPossibleAnswers;
	}
	public void setNumOfPossibleAnswers(int n)
	{
		for(Catagory c: this)
		{
			for(int i = 0; i < largestLength(); i++)
			{
				c.poss.get(i).ensureFor(n);
			}
		}
		numOfPossibleAnswers = n;

	}
	public int sum()
	{
		return size()*step*(largestLength()*(largestLength()-1)/2);
	}
	public void removeFromAll(int i)
	{
		for(Catagory c:this)
		{
			c.questions.remove(i);
			c.answers.remove(i);
			c.poss.remove(i);
		}
	}
}