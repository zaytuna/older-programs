import java.io.*;
import java.util.*;
import javax.swing.*;

public class Catagory
{
	static int length;
	String name;
	ArrayList<String> questions = new ArrayList<String>();
	ArrayList<String> answers = new ArrayList<String>();
	ArrayList<PossibleAnswers> poss = new ArrayList<PossibleAnswers>();

	public Catagory(String name, ArrayList<String> questions, ArrayList<String> answers)
	{
		this.name = name;
		this.questions = questions;
		this.answers = answers;
	}
	public Catagory(String n){this.name = n;}
	public static void writeToFile(CatagoryList catagories, String name)
	{
		try
		{
			File myFile = new File(new File(".").getAbsolutePath()+"\\Jeopardy Games\\"+name);
			System.out.println(myFile.getAbsolutePath());
			myFile.createNewFile();

			BufferedWriter out = new BufferedWriter(new FileWriter(myFile));

			// Header to file

			out.write(catagories.step+"\n");

			String catagoryNames = "";
			for(int z = 0; z < catagories.size(); z++)
			{
				catagoryNames += catagories.get(z).name+((z==catagories.size()-1)?"":"\t");
			}
			out.write(catagoryNames+"\n");

			// Questions to file

			for(int i = 0; i <catagories.largestLength(); i++)
			{
				String nextLine = "";
				for(int z = 0; z < catagories.size(); z++)
				{
					nextLine += catagories.get(z).questions.get(i)+((z==catagories.size()-1)?"":"\t");
				}
				out.write(nextLine+"\n");
				out.flush();
			}
			out.write("------------ANSWERS----------------------------\n");

			// Answers to file

			for(int i = 0; i <catagories.largestLength(); i++)
			{
				String nextLine = "";
				for(int z = 0; z < catagories.size(); z++)
				{
					nextLine += catagories.get(z).answers.get(i)+((z==catagories.size()-1)?"":"\t");
				}
				out.write(nextLine+"\n");
				out.flush();
			}


			// Possible Answers to file

			for(int v = 0; v < catagories.getNumOfPossibleAnswers(); v++)
			{
				out.write("-----------------------------------------------\n");
				for(int i = 0; i <catagories.largestLength(); i++)
				{
					String nextLine = "";
					for(int z = 0; z < catagories.size(); z++)
					{
						nextLine += catagories.get(z).poss.get(i).getValueAt(v)+((z==catagories.size()-1)?"":"\t");
					}
					out.write(nextLine+"\n");
					out.flush();
				}
			}
			out.close();


		}
		catch(IOException ioe)
		{
			ioe.printStackTrace();
		}
	}
	public static CatagoryList getAllFromFile()
	{
		try
		{
			CatagoryList cat = new CatagoryList();
			JFileChooser jfc = new JFileChooser();
			jfc.setCurrentDirectory(new File(new File(".").getAbsolutePath()+"\\Jeopardy Games\\"));
			int a = jfc.showOpenDialog(null);

			if(a == JFileChooser.APPROVE_OPTION)
			{
				String str =jfc.getSelectedFile().getAbsolutePath();
				File file = jfc.getSelectedFile();

				BufferedReader in = new BufferedReader(new FileReader(file));

				cat.step = Integer.parseInt(in.readLine());

				String list = in.readLine();
				String[] split = list.split("\t");

				for(String s: split)
				{
					Catagory c = new Catagory(s);
					cat.add(c);
				}

				int numDone = 0;
				int row = 0;

				while(true)
				{
					String temp = in.readLine();

					if(temp == null)
						break;

					if(temp.endsWith("****")||temp.endsWith("----"))
					{
						numDone++;
						row = 0;
						continue;
					}

					String[] blah = temp.split("\t");

					if(blah.length != split.length)
					{
						JOptionPane.showMessageDialog(null,"INCORRECT!\n\nEither number of rows in each column is different,"+
							"or the answers sheet doesn't match up with the file!"+
							"\nPlease correct for this mistake!\n\nL@ Line: "+temp+"\nIn catagory: "+numDone);
						System.exit(0);
					}
					for(int i = 0; i < split.length; i++)
					{
						if(numDone == 0)
						{
							cat.get(i).poss.add(new PossibleAnswers(-1));
							cat.get(i).questions.add(blah[i]);
						}
						else if(numDone == 1)
							cat.get(i).answers.add(blah[i]);
						else
							cat.get(i).poss.get(row).addValue(blah[i]);
					}
					row++;
				}
				in.close();
				return cat;
			}
			else
			{
				CreateJeopardy blah = new CreateJeopardy();
				cat = blah.waitTillDone();
				return cat;

			}
		}catch(IOException io)
		{
			io.printStackTrace();
		}

		return null;
	}
}
/*	public static CatagoryList getAllFromFile()
	{
		try
		{
			JOptionPane.showMessageDialog(null,"If you wish to play jeopardy from an existing file (created by this program),\n"+
				"      please select the file in the following dialog.\nIf you wish to create one, please press cancel.",
				"Opening Message",JOptionPane.PLAIN_MESSAGE,new ImageIcon("Opening.jpg"));

			CatagoryList cat = new CatagoryList();
			JFileChooser jfc = new JFileChooser();
			jfc.setCurrentDirectory(new File(new File(".").getAbsolutePath()+"\\Jeopardy Games\\"));
			int a = jfc.showOpenDialog(null);

			if(a == JFileChooser.APPROVE_OPTION)
			{
				String str =jfc.getSelectedFile().getAbsolutePath();
				File file = jfc.getSelectedFile();

				BufferedReader in = new BufferedReader(new FileReader(file));

				cat.numOfPossibleAnswers = Integer.parseInt(in.readLine());

				String list = in.readLine();
				String[] split = list.split("\t");

				for(String s: split)
				{
					Catagory c = new Catagory(s);
					cat.add(c);
				}

				int numDone = 0;

				while(true)
				{
					String temp = in.readLine();

					if(temp == null)
						break;

					if(temp.endsWith("****")||temp.endsWith("----"))
					{
						numDone++;
						continue;
					}

					String[] blah = temp.split("\t");

					if(blah.length != split.length)
					{
						JOptionPane.showMessageDialog(null,"INCORRECT!\n\nEither number of rows in each column is different,"+
							"or the answers sheet doesn't match up with the file!"+
							"\nPlease correct for this mistake!\n\nL@ Line: "+temp+"\nIn catagory: "+numDone);
						System.exit(0);
					}
					int row = 0;
					for(int i = 0; i < split.length; i++)
					{
						if(numDone == 0)
						{
							cat.get(i).poss.add(new PossibleAnswers(false));
							cat.get(i).questions.add(blah[i]);
						}
						else if(numDone == 1)
							cat.get(i).answers.add(blah[i]);
						else
						{
							cat.get(i).poss.get(row).setValue(-12,blah[i]);
							row++;
						}
					}
				}
				return cat;
			}
			else
			{
				CreateJeopardy blah = new CreateJeopardy();
				cat = blah.waitTillDone();
				return cat;

			}
		}catch(IOException io)
		{
			io.printStackTrace();
		}

		return null;
	}*/