import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.border.*;
import javax.imageio.*;
import java.util.concurrent.*;
import com.sun.jna.examples.*;

class CreateJeopardy extends JFrame implements ActionListener, TableModel, Runnable, ListSelectionListener, ChangeListener
{
	static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	CatagoryList decidedCats = new CatagoryList();
	boolean done = false;

	int[] columnWidths = {65,45,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220,220};

	JTable table;
	static TableModelListener listener;

	Color color = Color.BLUE;
	final int DIFF = 23;

	JButton addRow = new JButton("Add Point Value");
	JButton addColumn = new JButton("Add Catagory");
	JButton load = new JButton("Load Game");
	JButton quit = new GradientButton(Color.ORANGE,Color.RED);
	JButton setSteps = new JButton("Distance between values: 100");
	JButton save = new GradientButton(Color.BLUE,new Color(0,150,0));
	JSpinner changeNum = new JSpinner(new SpinnerNumberModel(3, 2, 10, 1));
	JScrollPane jsp;
	JLabel dataL = new JLabel("Multiple Choice: ",JLabel.RIGHT);
	ComponentImagePanel imgPanel;

	JCheckBox masked = new JCheckBox("Masked");
	JCheckBox oplacity = new JCheckBox("masked");
	JCheckBox resize = new JCheckBox("Automatic Resizeing");

	String[] data = {"Questions","Answers","Answer: A","Answer: B","Answer: C"};
	JList list = new MyList(data);
	JScrollPane listPane = new JScrollPane(list);

	Robot robot;
	boolean trans = false;
	Image img;
	File current = null;
	boolean saved = false;

	TableCellEditor defaultEditor;

	int modeTo;
	int wid = JeopardyDriver.screen.width-100-320;
	int jump = 100;
	final int DEFAULT;
	int numOfAnswers = 3;

	int mode = 1;

	//**************** JMENUS FOLLOW ************************
	ArrayList<JMenu> menus = new ArrayList<JMenu>();
	JMenu file = new JMenu("File");
		String[] namesFile = {"Exit","Save","Load","Rename","New","Print"};
		JMenuItem[] itemsFile = new JMenuItem[namesFile.length];


	CreateJeopardy()
	{
		//MENUS

		for(int i = 0; i < namesFile.length; i++)
		{
			itemsFile[i] = new JMenuItem(namesFile[i]);
			file.add(itemsFile[i]);
			itemsFile[i].addActionListener(new FileListener());
		}
		menus.add(file);

		//END MENUS

		quit.setText("Exit Program");
		save.setText("Save and Create");
		save.setForeground(Color.LIGHT_GRAY);
		setLayout(null);
		setBounds(50,150,JeopardyDriver.screen.width-100,(JeopardyDriver.screen.height-300)+DIFF);
		addRow.setBounds(100,5,125,25);
		addColumn.setBounds(225,5,110,25);
		dataL.setBounds(545,5,95,25);
		changeNum.setBounds(640,5,40,25);
		quit.setBounds(getWidth()-210,5,200,25);
		listPane.setBounds(getWidth()-210,200,210,120);
		setSteps.setBounds(335,5,210,25);
		masked.setBounds(getWidth()-210,320,200,25);
		resize.setBounds(getWidth()-210,345,200,25);
		save.setBounds(getWidth()-340,5,130,25);
		add(save);
		add(setSteps);
		add(addRow);
		add(changeNum);
		add(addColumn);
		add(dataL);
		add(quit);
		add(listPane);
		add(masked);
		add(resize);

		try
		{
			setIconImage(ImageIO.read(new File(new File(".").getAbsolutePath()+"Image.jpg")));
		}catch(IOException e){e.printStackTrace();}
		new MyMenus(this, menus);

		list.setSelectedIndex(0);

		addRow.addActionListener(this);
		addColumn.addActionListener(this);
		changeNum.addChangeListener(this);
		setSteps.addActionListener(this);
		list.addListSelectionListener(this);
		save.addActionListener(this);
		masked.addActionListener(this);
		masked.setOpaque(false);
		masked.setForeground(Color.ORANGE);
		resize.addActionListener(this);
		resize.setOpaque(false);
		dataL.setForeground(Color.GREEN);

		MotionListener mmm = new MotionListener(this);

		addMouseListener(mmm);
		addMouseMotionListener(mmm);
		resize.setForeground(Color.ORANGE);

		makeMask();

		quit.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent evt)
				{
					float x = 1f;
					System.out.println(decidedCats);
						while(x>.03f)
						{
							x-=.03;
							WindowUtils.setWindowAlpha(CreateJeopardy.this,x);
							try
							{
								Thread.sleep(20);
							}
							catch(InterruptedException e){}
						}
					System.exit(0);
				}
			}
		);

		table = new JTable(this);

		jsp = new JScrollPane(table);
		imgPanel = new ComponentImagePanel(jsp);
		imgPanel.setBounds(100,55,getWidth()-320,getHeight()-(90+DIFF));
		table.setBackground(new Color(224,204,152));
		DEFAULT = table.getAutoResizeMode();
		setUndecorated(true);

		if(trans)
		{
			masked.setSelected(true);
		}

		defaultEditor = table.getDefaultEditor(String.class);

		table.setDefaultRenderer(String.class,new MyCellRenderer());
		table.setDefaultEditor(String.class,new MyCellEditor());
		table.setRowHeight(30);
		table.setFont(new Font("SERIF",Font.PLAIN,14));
		getContentPane().setBackground(Color.BLACK);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		table.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

		add(imgPanel);
		setWidths();

		try
		{
			robot = new Robot();
		}
		catch(Exception e){}

		setVisible(true);
		table.addNotify();

		ExecutorService exc  = Executors.newCachedThreadPool();
		exc.execute(this);
		exc.shutdown();
	}
	public void paint(Graphics gb)
	{
		super.paint(gb);

		Graphics2D g = (Graphics2D)gb;
		g.setColor(color);
		g.setStroke(new BasicStroke(5,1,1));

		g.drawLine(getWidth()/2-60,55+DIFF,getWidth()/2-60,39+DIFF);//VERTICAL LINE ON TOP
		g.drawLine(getWidth()/2-60,getHeight()-35,getWidth()/2-60,getHeight()-5);//VERTICAL LINE ON BOTTOM

		g.drawLine(getWidth()-120,39+DIFF,getWidth()/2-60,39+DIFF);//HORIZONTAL LINE ON TOP
		g.drawLine(getWidth()-120,getHeight()-5,getWidth()/2-60,getHeight()-5);//HORIZONTAL LINE ON BOTTOM

		g.drawLine(getWidth()-120,39+DIFF,getWidth()-120,200+DIFF);//VERTICAL LINE ON SIDE FROM TOP TO LIST
		g.drawLine(getWidth()-120,getHeight()-5,getWidth()-120,370+DIFF);//VERTICAL LINE ON SIDE FROM BOTTOM TO LIST

		g.setPaint(Color.GREEN);
		g.drawRect(0,0,getWidth(),getHeight());
	}
	public void makeMask()
	{
		BufferedImage bi = new BufferedImage(getWidth(), getHeight(),BufferedImage.TYPE_INT_ARGB);
		BufferedImage all = new BufferedImage(getWidth(), getHeight(),BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.createGraphics();
		Graphics2D g2d = (Graphics2D)g;

		g2d.fillRect(0,0,1,1);
		g2d.fillRect(100,5+DIFF,getWidth()-200,25);//BUTTONS ON TOP
		g2d.fillRect(100+(getWidth()-320)/2-wid/2,55+DIFF,wid,getHeight()-(90+DIFF));// TABLE
		g2d.fillRect(getWidth()-210,200+DIFF,210,170);//LIST

		g2d.setStroke(new BasicStroke(5,1,1));
		g2d.drawLine(getWidth()/2-60,55+DIFF,getWidth()/2-60,39+DIFF);//VERTICAL LINE ON TOP
		g2d.drawLine(getWidth()/2-60,getHeight()-35,getWidth()/2-60,getHeight()-5);//VERTICAL LINE ON BOTTOM

		g2d.drawLine(getWidth()-120,39+DIFF,getWidth()/2-60,39+DIFF);//HORIZONTAL LINE ON TOP
		g2d.drawLine(getWidth()-120,getHeight()-5,getWidth()/2-60,getHeight()-5);//HORIZONTAL LINE ON BOTTOM

		g2d.drawLine(getWidth()-120,39+DIFF,getWidth()-120,getHeight()-5);// VERTICAL LINE STRAIGHT THROUGH

		g2d.dispose();
		g.dispose();

		g = all.createGraphics();
		g.fillRect(0,0,getWidth(),getHeight());
		g.dispose();

		System.setProperty("sun.java2d.noddraw", "true");
		//WindowUtils.setWindowAlpha(CreateJeopardy.this,.9f);
		if(trans)
			WindowUtils.setWindowMask(this, new ImageIcon(bi));
		else
			WindowUtils.setWindowMask(this, new ImageIcon(all));
		//WindowUtils.setWindowTransparent(this, true);
	}
	public void run()
	{
		try
		{
			Thread.sleep(300);
		}
		catch(InterruptedException e){}

		double num = 0;

		while(true)
		{
			if(mode == 0)
				num+=.05;
			else
				num=Math.PI+2;
			try
			{
				Thread.sleep(30);
			}
			catch(Exception e){}

			if(mode == 0)
				update(num);
		}
	}
	public static Color getColorAt(Color c1, Color c2, double ratio)
	{
		return new Color(c1.getRed()+(int)((c2.getRed()-c1.getRed())*ratio),
						c1.getGreen()+(int)((c2.getGreen()-c1.getGreen())*ratio),
						c1.getBlue()+(int)((c2.getBlue()-c1.getBlue())*ratio));
	}
	public void update(double x)
	{
		if(mode == 0)
		{
			wid = (int)(Math.abs(Math.sin(x))*(getWidth()-320));

			if(Math.abs(wid-(getWidth()-320))<2)
			{
				mode = modeTo;
				list.setEnabled(true);
				imgPanel.swapImage(null,(getWidth()-320-wid)/2+60,0,wid,(getHeight()-(90+DIFF)));
				return;
			}
			imgPanel.swapImage(img,(getWidth()-430-wid)/2+60,0,wid,(getHeight()-(90+DIFF)));
		}
		else
			jsp.setBounds(100,55,(getWidth()-320),(getHeight()-(90+DIFF)));
	}
	//*************** INTERFACE METHODS ARE AS FOLLOWS: *****************************

	public void actionPerformed(ActionEvent evt)
	{
		if(evt.getSource() == addColumn)
		{
			addColumn(JOptionPane.showInputDialog("Please enter the catagory name"));
		}
		else if(evt.getSource() == addRow)
		{
			addRow(-1);
		}
		else if(evt.getSource() == setSteps)
		{
			jump = Integer.parseInt(JOptionPane.showInputDialog("Please enter the new distance between values..."));
			setSteps.setText("Distance between values: "+jump);
			table.tableChanged(null);
			decidedCats.step = jump;
			setWidths();
		}
		else if(evt.getSource() == masked)
		{
			trans = masked.isSelected();
			makeMask();
		}
		else if(evt.getSource() == resize)
		{
			table.setAutoResizeMode(resize.isSelected()?DEFAULT:JTable.AUTO_RESIZE_OFF);
		}
		else if(evt.getSource() == save)
		{
			String str = JOptionPane.showInputDialog("Please Enter the name of this Jeopardy Game...");

			if(str!=null)
			{
				Catagory.writeToFile(decidedCats,str);
				saved = true;
				done = true;
				dispose();
			}
		}

	}
	public void stateChanged(ChangeEvent evt)
	{
		if(evt.getSource() == changeNum)
		{
			int val = (Integer)changeNum.getValue();
			ArrayList<String> ar = new ArrayList<String>();

			ar.add("Questions");
			ar.add("Answers");
			for(int z = 0; z < val; z++)
			{
				ar.add("Answer: "+(char)(z+65));
			}

			String[] d2 = new String[val+2];
			for(int i = 0; i < ar.size(); i++)
				d2[i] = ar.get(i);

			data=d2;
			list.setListData(data);
			list.setSelectedIndex(0);
			modeTo = 1;

			decidedCats.setNumOfPossibleAnswers(val);
		}
	}
	public void valueChanged(ListSelectionEvent evt)
	{
		if(mode != list.getSelectedIndex()+1)
		{
			modeTo = list.getSelectedIndex()+1;
			jsp.setVisible(false);
			list.setEnabled(false);
			img = robot.createScreenCapture(new Rectangle(getX()+100,getY()+55+DIFF,getWidth()-310,getHeight()-(90+DIFF)));
			mode = 0;
		}
	}
	//ャャャャャ�	THESE ARE THE ABSTRACT TABLEMODEL METHODS	ャャャャャャャ
	public void addTableModelListener(TableModelListener tl)
	{
		listener = tl;
	}
	public void removeTableModelListener(TableModelListener tl)
	{
	}
	public void setValueAt(Object aValue, int rowIndex, int columnIndex)
	{
		if(columnIndex > 1&&rowIndex>0)
		{
			switch(mode)
			{
				case 1:decidedCats.get(columnIndex-2).questions.set(rowIndex-1,(String)aValue);return;
				case 2:decidedCats.get(columnIndex-2).answers.set(rowIndex-1,(String)aValue);return;
			}
			decidedCats.get(columnIndex-2).poss.get(rowIndex-1).setValue(mode-3,(String)aValue);return;
		}
	}
	public boolean isCellEditable(int rowIndex, int columnIndex)
	{
		if(columnIndex == 0&&rowIndex>0)
		{
			if(JOptionPane.showConfirmDialog(this,"Are you sure you would like to delete this row?")==0)
			{
				decidedCats.removeFromAll(rowIndex-1);
				listener.tableChanged(null);
				setWidths();
			}
			return false;
		}
		if(rowIndex == 0&& columnIndex>=2)
		{
			if(JOptionPane.showConfirmDialog(this,"Are you sure you would like to delete this catagory?")==0)
			{
				decidedCats.remove(columnIndex-2);
				listener.tableChanged(null);
				setWidths();
			}
			return false;
		}

		return (rowIndex>=1)&&(columnIndex>=1);
	}
	public Object getValueAt(int rowIndex, int columnIndex)
	{
		if(columnIndex > 1&&rowIndex>0)
		{
			switch(mode)
			{
				case 1:return decidedCats.get(columnIndex-2).questions.get(rowIndex-1);
				case 2:return decidedCats.get(columnIndex-2).answers.get(rowIndex-1);
			}
			return decidedCats.get(columnIndex-2).poss.get(rowIndex-1).getValueAt(mode-3);
		}

		if(columnIndex>1)
			return "Remove catagory";

		if(columnIndex==1)
			return new Integer((rowIndex)*jump);
		else if(columnIndex == 0)
			return "Remove";

		return "Fail. This should never hapen!";
	}
	public int getRowCount()
	{
		return decidedCats.largestLength()+1;
	}
	public String getColumnName(int columnIndex)
	{
		if(columnIndex==0)
			return "REMOVE";
		if(columnIndex == 1)
			return "Points";

		return decidedCats.get(columnIndex-2).name;
	}
	public int getColumnCount()
	{
		return decidedCats.size()+2;
	}
	public Class<?> getColumnClass(int columnIndex)
	{
		//if(columnIndex==1)
		//	return Integer.class;
		return String.class;
	}
	//***************************************************
	public void addRow(int location)
	{
		decidedCats.addToAll("","",location);
		listener.tableChanged(new TableModelEvent(this));
	}
	public void addColumn(String ame)
	{
		if(decidedCats.size()<columnWidths.length)
		{
			Catagory catagory = new Catagory(ame);

			for(int i = 0; i < decidedCats.largestLength(); i++)
			{
				catagory.questions.add("?");
				catagory.answers.add("?");
				catagory.poss.add(new PossibleAnswers(-1));
			}

			decidedCats.add(catagory);
			listener.tableChanged(null);
			setWidths();
		}
	}
	public CatagoryList waitTillDone()
	{
		while(!done)
		{
			try
			{
				Thread.sleep(40);
			}catch(Exception e){}
		}
		done = false;
		return decidedCats;
	}
	public void setWidths()
	{
		for(int i = 0; i <table.getColumnModel().getColumnCount(); i++)
			table.getColumnModel().getColumn(i).setPreferredWidth(columnWidths[i]);

		ensure();
	}
	public void ensure()
	{
		for(int row = 0; row < table.getRowCount(); row++)
		{
			for(int column = 0; column<table.getColumnCount(); column++)
			{
				 Component cell = table.getDefaultRenderer(String.class)
					.getTableCellRendererComponent(table, table.getValueAt(row,column),
					false, false, row, column);
			}
		}
	}
	public class MotionListener extends MouseAdapter implements MouseMotionListener, MouseListener
	{
		Component owner;
		Point pressedAt = null;

		public MotionListener(Component c)
		{
			owner = c;
		}
		public void mousePressed(MouseEvent evt)
		{
			pressedAt = evt.getPoint();
		}
		public void mouseReleased(MouseEvent evt)
		{
			owner.setBounds(owner.getX()-pressedAt.x+evt.getX(),
				owner.getY()-pressedAt.y+evt.getY(),owner.getWidth(),owner.getHeight());
		}
		public void mouseDragged(MouseEvent evt)
		{
			owner.setBounds(owner.getX()-pressedAt.x+evt.getX(),
				owner.getY()-pressedAt.y+evt.getY(),owner.getWidth(),owner.getHeight());
		}
	}
	private class FileListener implements ActionListener
	{
		public void actionPerformed(ActionEvent evt)
		{
			//{"Exit","Save","Load","Rename","New","Print"};
			if(evt.getSource() == itemsFile[0])
			{
				System.exit(0);
			}
			else if(evt.getSource() == itemsFile[1])
			{
				String str = JOptionPane.showInputDialog("Please Enter the name of this Jeopardy Game...");

				if(str!=null)
				{
					Catagory.writeToFile(decidedCats,str);
					saved = true;
					current = new File(new File(".").getAbsolutePath()+"\\Jeopardy Games\\"+str);
				}
			}
			else if(evt.getSource() == itemsFile[2])
			{
				decidedCats = Catagory.getAllFromFile();
				listener.tableChanged(null);
			}
			else if(evt.getSource() == itemsFile[3])
			{
				String str = JOptionPane.showInputDialog("Please Enter the name of this Jeopardy Game...",saved?
					current.getName():"");

				current.delete();
				Catagory.writeToFile(decidedCats,str);
				saved = true;
			}
			else if(evt.getSource() == itemsFile[4])
			{
				int x = JOptionPane.showConfirmDialog(CreateJeopardy.this, "Would you like to save?");
				if(x == 2)
					return;
				if(x == 0)
				{
					String str = JOptionPane.showInputDialog("Please Enter the name of this Jeopardy Game...");

					if(str!=null)
					{
						Catagory.writeToFile(decidedCats,str);
						saved = true;
						current = new File(new File(".").getAbsolutePath()+"\\Jeopardy Games\\"+str);
					}
				}

				decidedCats = new CatagoryList();
				listener.tableChanged(null);

				saved = false;
			}
			else if(evt.getSource() == itemsFile[5])
			{
				try
				{
					table.print();
				}
				catch(Exception e){e.printStackTrace();}
			}
		}
	}
	private class MyCellRenderer extends DefaultTableCellRenderer
	{
		public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column)
		{

			Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			//if(!hasFocus)
			{
				if((getColumnName(column).equals("REMOVE")||getColumnName(column).equals("Points"))||row == 0)
				{
					cell.setBackground(new Color(0,195,196));
				}
				else
				{
					cell.setBackground(new Color(224,204,152));
					cell.setForeground(Color.BLACK);
					((JComponent)cell).setBorder(null);
				}
			}
			if(isSelected)
			{
				table.setRowHeight(row,50);
				table.setRowHeight(row,50);
				((JComponent)cell).setBorder(new CompoundBorder(new SoftBevelBorder(
						BevelBorder.LOWERED),new LineBorder(Color.BLUE,5)));
			}
			else
				table.setRowHeight(row,30);

			return cell;
		}
	}

	private class MyCellEditor implements TableCellEditor
	{
		JSpinner spinner = new JSpinner(new SpinnerNumberModel(0,-Math.pow(10,9),Math.pow(10,9),decidedCats.step));
		int col = -1;

		public Component getTableCellEditorComponent(JTable table, Object value,boolean isSelected,int row,int column)
		{
			col = column;
			if(column == 1)
			{
				spinner.setValue(value);
				return spinner;
			}
			return new JScrollPane(new JTextArea());
			//return defaultEditor.getTableCellEditorComponent(table,value,isSelected,row,column);
		}
		public Object getCellEditorValue()
		{
			System.out.println(col);

			if(col == 1)
				return spinner.getValue();

			return defaultEditor.getCellEditorValue();
		}
		public void addCellEditorListener(CellEditorListener l){}
		public void removeCellEditorListener(CellEditorListener l) {}
		public void cancelCellEditing(){}
		public boolean isCellEditable(EventObject anEvent)
		{
			if(anEvent instanceof MouseEvent)
			{
				if(((MouseEvent)anEvent).getClickCount()>1)
					return true;
			}
			return false;
		}
		public boolean shouldSelectCell(EventObject anEvent){return false;}
		public boolean stopCellEditing(){return true;}
	}

	public static void main(String args[])
	{
		new CreateJeopardy();
	}
}
