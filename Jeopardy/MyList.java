import java.awt.event.*;
import java.awt.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.border.*;

class MyList extends JList implements ActionListener
{
	Point ball = new Point(50,50);
	int xRate = -5;
	int yRate = (int)(Math.random()*16)-8;
	Timer t = new Timer(50,this);

	MyList(Object[] data)
	{
		super(data);
		setBorder(new LineBorder(Color.BLUE));
		t.start();
		setBackground(new Color(140,0,0));
		setForeground(Color.WHITE);
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(new Color(0,255,0,150));
		g.fillOval(ball.x-15,ball.y-15,30,30);
		g.setColor(Color.black);
		g.drawOval(ball.x-15,ball.y-15,30,30);
		ball.x+=xRate;
		ball.y+=yRate;
		if(ball.x<15||ball.x>getWidth()-15)
			xRate*=-1;
		if(ball.y<15||ball.y>getHeight()-15)
			yRate*=-1;
	}
	public void actionPerformed(ActionEvent evt)
	{
		repaint();
	}
}