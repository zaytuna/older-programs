import java.awt.Color;
class Player
{
	java.awt.Color color;
	int points;
	int index;
	String name;
	PlayerComponent representation;

	Player(String name, Color c,ScoreBoard b)
	{
		color = c;
		representation = new PlayerComponent(this ,b);
		this.name = name;
	}
	Player(ScoreBoard b)
	{
		this("",new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)),b);
	}
}