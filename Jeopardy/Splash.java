import java.applet.*;
import java.util.*;
import java.text.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import javax.imageio.*;
import java.io.*;
import javax.swing.*;
//import com.sun.jna.examples.WindowUtils;


public class Splash extends JFrame implements KeyListener
{
	Toolkit tools = Toolkit.getDefaultToolkit();
	Dimension screen = tools.getScreenSize();
	String string1 = "  Welcome to the Jeopardy Game";
	String string2 = "Please press enter or escape to continue...";
	int numberOfRings = 1;

	ArrayList<Point> begin = new ArrayList<Point>();
	String s1 = "";
	String s2 = "";
	int progress = -1;
	boolean grad = false;
	boolean done = false;
	boolean finishedWithWindow=false;
	ArrayList<Integer>  size = new ArrayList<Integer>();
	javax.swing.Timer timeDelay = new javax.swing.Timer(10,new Chicken());

	ArrayList<Color> theColorC1 = new ArrayList<Color>();
	ArrayList<Color> theColorC2 = new ArrayList<Color>();
	float ratio = 0;
	int variation = 7;
	int scheme = 0;
	Paint[] foregrounds = {Color.red,new GradientPaint(0,0,Color.orange,300,30,new Color(0,0,100)),
		Color.green,Color.green,new GradientPaint(0,0,Color.blue,screen.width,0,new Color(100,0,0))};
	Paint[] backgrounds = {Color.black,new GradientPaint(0,0,Color.black,300,30,new Color(170,200,255)),
		new Color(0,0,150),Color.black,new GradientPaint(0,0,Color.orange,screen.width,0,Color.pink)};


	class MyPanel extends Colorful
	{

		MyPanel()
		{
			super(Color.black,new Color(170,200,255),new Point(300,30));
			for(int x = 0; x < numberOfRings; x++)
			{
				size.add(new Integer(50));
				begin.add(new Point((int)(Math.random()*screen.width),
							(int)(Math.random()*screen.height)));
				theColorC2.add(new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
				theColorC1.add(new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
			}
		}

		public void paint(Graphics g)
		{
			try
			{
				super.paint(g);
				Graphics2D g2 = (Graphics2D)g;
				super.setPaint(backgrounds[scheme]);

				if(progress >= string1.length())
					done = true;
				try
				{
					if(!done)
						s1 += string1.charAt(progress);
					else
						s2 += string2.charAt(progress-string1.length());
				}catch(Exception e){}
				ratio += 0.03;

				if(done&& progress >= string1.length()+string2.length())
				{
					for(int v = 0; v < numberOfRings; v++)
					{

						if(size.get(v) >= 2*Math.sqrt(Math.pow(screen.height,2)+Math.pow(screen.width,2)))
							size.set(v,new Integer(50));
						if(size.get(v) == 50)
						{
							begin.set(v,new Point((int)(Math.random()*screen.width),
							((int)(Math.random()*screen.height))));
						}
						/*System.out.println(explosionHeight);
						if(FireBox.y > explosionHeight)
						{
							g2.setPaint(new GradientPaint(FireBox.x,FireBox.y,Color.RED,FireBox.x+XSIZE,
								FireBox.y+YSIZE,Color.BLUE));
							g2.fillRect(FireBox.x,FireBox.y,XSIZE,YSIZE);
							g2.drawLine(FireBox.x+(XSIZE/2),FireBox.y+YSIZE,FireBox.x+(XSIZE/2),FireBox.y+YSIZE+40);
							explosionHeight = (int)(Math.random()*300);
							repaint();
						}
						else if(FireBox.y == explosionHeight)
						{
							explode = true;

						}*/

						if(ratio > 1)
						{
							ratio = 0;

							theColorC1.set(v,theColorC2.get(v));
							theColorC2.set(v,new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
						}
						g2.setStroke(new BasicStroke(variation*(v+1),BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
						Color myColor = new Color(Math.min((int)(theColorC1.get(v).getRed()+(theColorC2.get(v).getRed()-theColorC1.get(v).getRed())*ratio),255),
							Math.min((int)(theColorC1.get(v).getGreen()+(theColorC2.get(v).getGreen()-theColorC1.get(v).getGreen())*ratio),255),
							Math.min((int)(theColorC1.get(v).getBlue()+(theColorC2.get(v).getBlue()-theColorC1.get(v).getBlue())*ratio),255));
						Color myColor2 = new Color((myColor.getRed()+127)%255,
							(myColor.getGreen()+127)%255,(myColor.getBlue()+127)%255);

						g2.setPaint(grad?new GradientPaint(0,0,myColor,300,100,myColor2,true):myColor);
						g2.drawOval(begin.get(v).x,begin.get(v).y,size.get(v),size.get(v));
					}
				}

				g2.setPaint(foregrounds[scheme]);
				Font f = new Font("ANDY",Font.BOLD,65);
				g2.setFont(f);
				g2.drawString(s1,screen.width-1200,300);
				Font a = new Font("SERIF",Font.ITALIC,30);
				g2.setFont(a);
				g2.drawString(s2,screen.width-1000,screen.height-300);
			}
			catch(Exception e){System.out.println(e+"\tIn paint method");}
		}


	}
	JPanel p;
	float x = .9f;
	/**
	*/
	Splash()
	{
		p = new MyPanel();
		add(p);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		timeDelay.start();
		//System.setProperty("sun.java2d.noddraw", "true");
		//WindowUtils.setWindowAlpha(this, .8f);
		try
		{
			setIconImage(ImageIO.read(new File(new File(".").getAbsolutePath()+"Image.jpg")));
		}catch(IOException e){e.printStackTrace();}
		addKeyListener(this);
		setUndecorated(true);
		setAlwaysOnTop(true);
		setBackground(Color.WHITE);
		setBounds(0,0,screen.width,screen.height);
		setVisible(true);//last line
	}
	/**
	*/
	public void waitTillDone()
	{
		while(!finishedWithWindow)
		{
			try
			{
				Thread.sleep(50);
			}
			catch(Exception e){}
		}
		finishedWithWindow = false;
		return;
	}
	public void keyReleased(KeyEvent evt){}public void keyTyped(KeyEvent evt){}
	public void keyPressed(KeyEvent evt)
	{
		String key = KeyEvent.getKeyText(evt.getKeyCode());
		System.out.println(key);

		if(key.equals("Enter") || key.equals("Escape")||key.equals("Entr�e")||key.equals("Esc"))
		{
			setVisible(false);
			finishedWithWindow = true;
			dispose();
		}
		else if(key.equals("Space"))
		{
			grad = !grad;
		}
		else if(key.equals("Up"))
		{
			numberOfRings++;
			size.add(50);
			begin.add(new Point((int)(Math.random()*screen.width),
				(int)(Math.random()*screen.height)));
			theColorC2.add(new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
				theColorC1.add(new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
		}
		else if(key.equals("Down")&&(numberOfRings>0))
		{
			numberOfRings--;
			size.remove(numberOfRings-1);
			begin.remove(numberOfRings-1);
			theColorC1.remove(numberOfRings-1);
			theColorC2.remove(numberOfRings-1);

			if(numberOfRings == 0)
			{
				size.clear();
				begin.clear();
				theColorC1.clear();
				theColorC2.clear();
			}
		}
		else if(key.equals("Open Bracket"))
		{
			if(scheme < foregrounds.length)
				scheme++;
		}
		else if(key.equals("Close Bracket"))
		{
			if(scheme >0)
				scheme--;
		}
		else if(key.equals("Left")&&variation>=1)
		{
			variation-=2;
		}
		else if(key.equals("Right")&&variation<50)
		{
			variation+=2;
		}
		else if(key.equals("NumPad-4")&&x<.99)
		{
			x+=.01;
			//WindowUtils.setWindowAlpha(this, x);
		}
		else if(key.equals("NumPad-6")&&x>.01)
		{
			x-=.01;
			//WindowUtils.setWindowAlpha(this, x);
		}


	}
	public static void main(String args[])//for testing purposes
	{
		try
		{
			new Splash();
		}
		catch(Exception e){e.printStackTrace();System.exit(0);}
	}
	public class Chicken implements ActionListener
	{
		public void actionPerformed(ActionEvent evt)
		{
			for(int i = 0; i < numberOfRings; i++)
			{
				size.set(i,size.get(i)+10);
				begin.set(i,new Point(begin.get(i).x-5,begin.get(i).y-5));
			}
			if(progress < string1.length()+string2.length())
				progress++;
			repaint();
		}
	}
}