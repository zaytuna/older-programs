import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.border.*;

class PlayerComponent extends Colorful implements ActionListener
{
	Player p;

	JButton colorB = new JButton("");
	JTextField nameF = new JTextField();
	JLabel pointL = new JLabel("Points: 0");
	JButton remove = new JButton("Remove");
	ScoreBoard owner;

	String name = "";

	PlayerComponent(Player p, ScoreBoard b)
	{
		super(null,Color.WHITE,new Point(110,35));
		this.p = p;
		this.owner = b;

		setLayout(null);
		setBorder(new SoftBevelBorder(BevelBorder.RAISED));
		setOpaque(false);

		remove.setFont(new Font("SERIF",Font.PLAIN,11));
		nameF.setFont(new Font("SERIF",Font.BOLD,13));
		colorB.setBackground(p.color);

		colorB.setBounds(25,30,25,25);
		nameF.setBounds(70,3,82,25);
		pointL.setBounds(55,30,85,25);
		remove.setBounds(3,3,67,25);
		colorB.setOpaque(true);

		add(remove);
		add(pointL);
		add(nameF);
		add(colorB);

		remove.addActionListener(this);
		nameF.addActionListener(this);
		colorB.addActionListener(this);
	}
	public void updateInfo()
	{
		pointL.setText("Points: "+p.points);
		colorB.setBackground(p.color);
		nameF.setText(p.name);
	}
	public void actionPerformed(ActionEvent evt)
	{
		if(evt.getSource() == remove)
		{
			owner.remove(this);
			owner.players.remove(p);
			owner.arrangeComponents();
			owner.owner.repaint();
			System.gc();
		}
		else if(evt.getSource() == nameF)
		{
			p.name = nameF.getText();
			name = p.name;
			nameF.setOpaque(false);
			super.c1 = p.color;
			if(owner.players.indexOf(this.p)==0)
				owner.owner.enableButtons(true);
			super.paint();
			setOpaque(true);
			repaint();
			nameF.repaint();
			owner.update();
			super.paint();
			owner.owner.repaint();
		}
		else if(evt.getSource() == colorB)
		{
			Color c = JColorChooser.showDialog(null, "Choose a player color", p.color);
			p.color = c;
			super.c1 = c;
			super.c2 = Color.WHITE;
			colorB.setBackground(c);

			if(name!=null&&(!name.equals("")))
				super.paint();

			owner.owner.repaint();
		}
	}
}