import java.awt.*;
import java.awt.color.*;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;

class WorksPanel extends Colorful implements Runnable
{
	static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	public static final int WIDTH = screen.width-200;
	public static final int HEIGHT = screen.height-100;

	MemoryImageSource source;
	Image image;

	byte[] bytes = new byte[WIDTH*HEIGHT];

	ArrayList<Firework> fireworks = new ArrayList<Firework>();

	boolean on = false;
	String message = "";
	Color color = Color.BLACK;

	WorksPanel(Color c1,Color c2,Point p,Point p2, String message, Color col)
	{
		super(c1,c2,p,p2);

		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		this.message = message;
		this.color = col;

		Arrays.fill(bytes,(byte)0);

		byte[] r = new byte[127];
		byte[] g = new byte[127];
		byte[] b = new byte[127];
		byte[] a = new byte[127];

		for(int i = 0; i < 127; i++)
		{
			Color c = new Color(Color.HSBtoRGB(.1673f,1,1));
			r[i] = (byte)c.getRed();
			g[i] = (byte)c.getGreen();
			b[i] = (byte)c.getBlue();
			a[i] = (byte)(i*2);
		}
			//r[i] = g[i] = b[i] = (byte)(i*2);

		source = new MemoryImageSource(WIDTH,HEIGHT,new IndexColorModel(8,127,r,g,b,a),bytes,0,WIDTH);
		source.setAnimated(true);
		source.setFullBufferUpdates(true);

		image = createImage(source);

		setVisible(true);

		Thread t = new Thread(this);
		t.start();
	}
	WorksPanel(String m, Color ca)
	{
		this(null,null,null,null,m, ca);
	}
	WorksPanel(Color color1, Color color2)
	{
		this(color1,color2,null,null,"", Color.GREEN);
	}
	WorksPanel(Color c1, Color c2, String m, Color ca)
	{
		this(c1,c2,null,null,m,ca);
	}
	WorksPanel(Color c1, Color c2, Point p, String m, Color ca)
	{
		this(c1,c2,p,null,m,ca);
	}

	public void run()
	{
		while(true)
		{
			repaint();

			try
			{
				Thread.sleep(30);
			}
			catch(Exception e){}
		}
	}

	public void setFireworks(boolean b)
	{
		on = b;
	}

	public void update()
	{
		Arrays.fill(bytes,(byte)0);

		if(Math.random() > .90)
			fireworks.add(new Firework());

		for(Firework f: fireworks)
		{
			f.update();

			if(f.phase == 0)
			{
				int xm = (int)f.x+f.r;
				int ym = (int)f.y+f.r;

				for(int x = (int)f.x-f.r; x < xm; x++)
					for(int y = (int)f.y-f.r; y < ym; y++)
						if(x+y*WIDTH > 0 && x+y*WIDTH < WIDTH*HEIGHT)
							bytes[x+y*WIDTH] = (byte)Math.min(Math.abs(bytes[x+y*WIDTH]+(Math.max(0,f.r-f.distance(x,y)))*f.timeR),126);
			}
			else if(f.phase == 1)
			{
				for(Particle p : f.particles)
				{
					if(p.timeR < 0)
						continue;

					int xm = (int)p.x+p.r;
					int ym = (int)p.y+p.r;

					for(int x = (int)p.x-p.r; x < xm; x++)
						for(int y = (int)p.y-p.r; y < ym; y++)
							if(x+y*WIDTH > 0 && x+y*WIDTH < WIDTH*HEIGHT)
								bytes[x+y*WIDTH] = (byte)Math.min(Math.abs(bytes[x+y*WIDTH]+(Math.max(0,p.r-p.distance(x,y)))*p.timeR),126);
				}
			}
		}

		for(int i = 0; i < fireworks.size(); i++)
			if(fireworks.get(i).phase == 2)
				fireworks.remove(i);
	}

	public void paint(Graphics g)
	{
		super.paint(g);

		if(on)
		{
			update();
			source.newPixels();
			g.drawImage(image,0,0,getWidth(),getHeight(),null);
		}
		g.setColor(color);
		g.setFont(new Font("SANS_SERIF",Font.BOLD+Font.ITALIC,40));
		g.drawString(message,getWidth()/2-message.length()*10,getHeight()/2);
	}

	public static void main(String args[])
	{
		JOptionPane.showMessageDialog(null,new WorksPanel(Color.BLUE,Color.BLACK));
	}
}