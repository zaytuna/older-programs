import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.awt.event.*;
import java.util.*;
import javax.imageio.*;


class JeopardyDriver extends JFrame implements ActionListener, Runnable
{
	static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	JButton[][] buttons;
	static int turn = 0;
	CatagoryList catagories;
	ScoreBoard board  = new ScoreBoard(this);
	JScrollPane sPane = new JScrollPane(board);
	JPanel panel = new JPanel();
	boolean gameOver = false;

	JeopardyDriver(boolean intro)
	{
		try
		{
			setIconImage(ImageIO.read(new File(new File(".").getAbsolutePath()+"Image.jpg")));
			JOptionPane.showMessageDialog(null,"","",0,new ImageIcon(ImageIO.read(new File(new File(".").getAbsolutePath()+
				"Image.jpg"))));
		}catch(IOException e){e.printStackTrace();}

		if(intro)
		{
			Splash s =new Splash();
			s.waitTillDone();

			JOptionPane.showMessageDialog(null,"If you wish to play jeopardy from an existing file (created by this program),\n"+
				"      please select the file in the following dialog.\nIf you wish to create one, please press cancel.",
				"Opening Message",JOptionPane.PLAIN_MESSAGE,new ImageIcon("Opening.jpg"));
		}

		catagories = Catagory.getAllFromFile();
		if(catagories == null)
			dispose();
		else
		{
			buttons = new JButton[catagories.size()][catagories.largestLength()];
			setLayout(new BorderLayout());
			panel.setLayout(new GridLayout(catagories.largestLength()+1,catagories.size()));
			add(panel,BorderLayout.CENTER);
			add(sPane,BorderLayout.EAST);

			//System.out.println("largestLenght (rows): "+catagories.largestLength()+"\t\tsize (columns): "+catagories.size());

			for(Catagory jkfla:catagories)
			{
				JLabel l = new JLabel(jkfla.name, JLabel.CENTER);
				l.setFont(new Font("SERIF",Font.ITALIC+Font.BOLD,18));
				panel.add(l);
			}
			for(int len = 0; len < catagories.largestLength(); len++)
			{
				for(int cat = 0; cat < catagories.size(); cat++)
				{
					buttons[cat][len] = new JButton((catagories.step*(len+1))+": "+catagories.get(cat).name);
					buttons[cat][len].addActionListener(this);
					panel.add(buttons[cat][len]);
				}
			}
			enableButtons(false);
			centerFrame(900,600,this);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setVisible(true);
		}
	}
	public Player[] winningIndexes()
	{
		ArrayList<Player> al = new ArrayList<Player>();

		int max = -50*catagories.sum();
		System.out.println(max);

		for(Player p : board.players)
		{
			if(p.points>max)
			{
				al.clear();
				al.add(p);
				max = p.points;
			}
			else if(p.points == max)
				al.add(p);
		}

		Player[] al2 = new Player[al.size()];
		for(int j = 0; j < al.size(); j++)
			al2[j] = al.get(j);

		return al2;
	}
	public void run()
	{
		Player[] win = winningIndexes();

		if(win.length == 1)
			new Fireworks(win[0].name+" wins!",win[0].color);
		else
		{
			String st = "";
			for(int x = 0; x < win.length-1; x++)
			{
				Player i = win[x];
				st += i.name+", ";
			}
			st += "and "+win[win.length-1].name+" tie for first place!";
			new Fireworks(st,randomColor());
		}
		new JeopardyDriver(false);
	}
	public static Color randomColor()
	{
		return new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255));
	}
	public static Object[] shuffle(Object[] stuff)
	{
		for(int i = 0; i < stuff.length*5; i++)
			swap(stuff,(int)(Math.random()*stuff.length),(int)(Math.random()*stuff.length));

		return stuff;
	}
	public static void swap(Object[] stuff, int posA,int posB)
	{
		Object inA = stuff[posA];
		Object inB = stuff[posB];
		stuff[posA] = inB;
		stuff[posB] = inA;
	}
	public void actionPerformed(ActionEvent evt)
	{
		for(int i = 0; i < catagories.size(); i++)
		{
			for(int j = 0; j < catagories.largestLength(); j++)
			{
				if(evt.getSource() == buttons[i][j])
				{
					ArrayList<String> ar = catagories.get(i).poss.get(j).getValues();
					if(ar.indexOf(catagories.get(i).answers.get(j))<0)
						ar.add(catagories.get(i).answers.get(j));

					Object options[] = shuffle(ar.toArray());
					int ans = JOptionPane.showOptionDialog(this,
						catagories.get(i).questions.get(j),"Question",JOptionPane.DEFAULT_OPTION,
						JOptionPane.QUESTION_MESSAGE,new ImageIcon("QUESTION.jpg"), options, options[0]);

					if(catagories.get(i).answers.get(j).equals(options[ans]))
					{
						buttons[i][j].setEnabled(false);
						board.players.get(turn).points += catagories.step*(j+1);
					}
					else
						board.players.get(turn).points -= catagories.step*(j+1);

					turn = (turn+1)%board.players.size();
					board.update();

					gameOver = true;
					for(int r = 0; r < catagories.size(); r++)
					{
						for(int k = 0; k < catagories.largestLength(); k++)
						{
							if(buttons[r][k].isEnabled())
							{
								gameOver= false;
								break;
							}
						}
					}
					if(gameOver)
					{
						dispose();
						new Thread(this).start();
					}
				}
			}
		}
	}
	public void enableButtons(boolean b)
	{
		for(int len = 0; len < catagories.largestLength(); len++)
		{
			for(int cat = 0; cat < catagories.size(); cat++)
			{
				buttons[cat][len].setEnabled(b);
			}
		}
	}
	public static void centerFrame(int w, int h, Component c)
	{
		int x = (screen.width-w)/2;
		int y = (screen.height-h)/2;

		c.setBounds(x,y,w,h);
	}
	public static void main(String[] args)
	{
		new JeopardyDriver(true);
	}
}