import java.util.*;

class Firework
{
	double 	x,
			y,
			xV,
			yV;

	double c,cx,cy;

	int r = 5;
	double timeR = 20;

	int phase = 0; //0 = launching 1 = exploded 2 = dead

	Particle[] particles = new Particle[2000];

	Firework()
	{
		x = 100+Math.random()*(WorksPanel.WIDTH-100);
		y = WorksPanel.HEIGHT+Math.random()*100;
		yV = -Math.random()*20-15;
		xV = Math.random()*4-2;
	}

	public double distance(int x1,int y1)
	{
		return Math.sqrt(c+x1*(cx+x1)+y1*(cy+y1));
	}

	public void update()
	{
		timeR = Math.max(0,timeR-.1);

		if(phase != 1 && y < WorksPanel.HEIGHT/2 && (Math.random() > .9 || (y < WorksPanel.HEIGHT/5 && Math.random() < .5)))
		{
			for(int i = 0; i < particles.length; i++)
				particles[i] = new Particle(this);

			phase = 1;
		}

		if(phase == 0)
		{
			x += xV;
			y += yV += .15;

			c = x*x+y*y;
			cx = -2*x;
			cy = -2*y;
		}
		else if(phase == 1)
		{
			for(Particle p : particles)
				p.update();

			if(particles[0].timeR < .001)
				phase = 2;
		}
	}
}