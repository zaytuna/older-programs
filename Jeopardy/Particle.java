class Particle
{
	double 	x,
			y,
			xV,
			yV;

	double c,cx,cy;

	int r = 2;
	double ti = 0;
	double acc = .9;

	double timeR = 30;
	double fallout = Math.random()/100;
	int count = 0;

	Particle(Firework parent)
	{
		x = parent.x;
		y = parent.y;

		double t = Math.random()*Math.PI*2;
		double m = Math.random()*10;

		yV = m*Math.sin(t);
		xV = m*Math.cos(t);
	}

	public double distance(int x1,int y1)
	{
		return Math.sqrt(c+x1*(cx+x1)+y1*(cy+y1));
	}

	public void update()
	{
		ti+=Math.random();
		count++;
		timeR -= fallout*count*count;

		x += xV;
		y += yV + acc*ti;

		c = x*x+y*y;
		cx = -2*x;
		cy = -2*y;
	}
}