import javax.swing.*;
import java.awt.*;

class ComponentImagePanel extends JPanel
{
	Image img;
	int[] bounds = new int[4];
	JScrollPane p;

	ComponentImagePanel(JScrollPane pane)
	{
		super(true);
		this.p = pane;

		setLayout(new BorderLayout());
		add(pane, BorderLayout.CENTER);
		setOpaque(false);
		setBackground(Color.RED);
	}
	public void swapImage(Image i,int x, int y, int w, int h)
	{
		img = i;
		bounds[0] = x;
		bounds[1] = y;
		bounds[2] = w;
		bounds[3] = h;

		if(i == null)
			p.setVisible(true);
		else
			p.setVisible(false);

		repaint();
	}
	public void paint(Graphics g)
	{
		super.paint(g);

		if(img!=null)
		{
			g.drawImage(img,bounds[0],bounds[1],bounds[2],bounds[3],this);
		}
	}

}