import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class Fireworks extends JFrame implements ActionListener
{
	JButton exit = new JButton("Exit");
	boolean done = false;
	static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	Fireworks(String s, Color c)
	{
		WorksPanel wp = new WorksPanel(Color.BLACK,new Color(0,0,100),s,c);
		wp.setFireworks(true);
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout(FlowLayout.CENTER));
		panel.add(exit);
		exit.addActionListener(this);
		getContentPane().setBackground(Color.BLACK);

		add(wp,BorderLayout.CENTER);
		add(panel,BorderLayout.SOUTH);
		panel.setOpaque(false);

		MotionListener ml = new MotionListener(this);

		addMouseListener(ml);
		addMouseMotionListener(ml);

		setUndecorated(true);

		//pack();
		centerFrame(wp.WIDTH,wp.HEIGHT+30);

		setVisible(true);
		setAlwaysOnTop(true);

		waitTillDone();
	}
	public void actionPerformed(ActionEvent evt)
	{
		done = true;
	}
	public void centerFrame(int frameWidth, int frameHeight)
	{
		int xUpperLeftCorner = (screen.width-frameWidth)/2;
		int yUpperLeftCorner = (screen.height-frameHeight)/2;

		setBounds(xUpperLeftCorner,yUpperLeftCorner,frameWidth,frameHeight);
	}
	public class MotionListener extends MouseAdapter implements MouseMotionListener, MouseListener
	{
		Component owner;
		Point pressedAt = null;

		public MotionListener(Component c)
		{
			owner = c;
		}
		public void mousePressed(MouseEvent evt)
		{
			pressedAt = evt.getPoint();
		}
		public void mouseReleased(MouseEvent evt)
		{
			owner.setBounds(owner.getX()-pressedAt.x+evt.getX(),
				owner.getY()-pressedAt.y+evt.getY(),owner.getWidth(),owner.getHeight());
		}
		public void mouseDragged(MouseEvent evt)
		{
			owner.setBounds(owner.getX()-pressedAt.x+evt.getX(),
				owner.getY()-pressedAt.y+evt.getY(),owner.getWidth(),owner.getHeight());
		}
	}
	public void waitTillDone()
	{
		while(!done)
		{
			try
			{
				Thread.sleep(50);
			}
			catch(InterruptedException ie)
			{
			}
		}
		done = false;
		dispose();
	}
	public static void main(String[] args)
	{
		new Fireworks("MESSAGE",Color.ORANGE);
	}
}