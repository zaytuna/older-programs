import javax.swing.*;
import java.awt.*;
import com.sun.jna.examples.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.util.*;

class MyMenus extends JMenuBar implements ActionListener, ChangeListener
{
	JFrame owner;

	JMenu looks = new JMenu("Looks");

	JPanel trans = new Colorful(Color.WHITE,Color.ORANGE);
	JLabel oL = new JLabel("Oplacity: 90");
	JSlider oplacity = new JSlider(JSlider.HORIZONTAL,0,100,90);

	JButton background = new JButton("Background Color");

	MyMenus(JFrame own, ArrayList<JMenu> menus)
	{
		super();
		this.owner = own;

		trans.setLayout(new BorderLayout());
		trans.add(oplacity,BorderLayout.CENTER);
		trans.add(oL, BorderLayout.NORTH);

		if(menus != null)
		{
			for(JMenu m : menus)
			{
				this.add(m);
				m.addMenuListener(new Painter());
			}
		}

		oplacity.addChangeListener(this);
		oplacity.setOpaque(false);
		background.addActionListener(this);

		own.setJMenuBar(this);
		this.add(looks);
		looks.add(trans);
		looks.add(background);

		looks.addMenuListener(new Painter());
	}
	MyMenus(JFrame own)
	{
		this(own,null);
	}
	public void stateChanged(ChangeEvent evt)
	{
		WindowUtils.setWindowAlpha(owner,(float)oplacity.getValue()/100);
		oL.setText("Oplacity: "+oplacity.getValue());
	}
	public void actionPerformed(ActionEvent evt)
	{
		if(evt.getSource() == background)
		{
			Color color = JColorChooser.showDialog(background, "Choose a player color", owner.getContentPane().getBackground());
			background.setBackground(color);
			background.setForeground(new Color((color.getRed()+127)%255,(color.getGreen()+127)%255,(color.getBlue()+127)%255));
			owner.getContentPane().setBackground(color);
		}
	}
	private class Painter implements MenuListener
	{
		public void menuCanceled(MenuEvent e)
		{
			owner.repaint();
		}
		public void menuDeselected(MenuEvent evt){owner.repaint();}
		public void menuSelected(MenuEvent evt){}
	}
}