import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.awt.event.*;

public class Tetris extends JFrame implements Runnable, KeyListener
{
	public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();

	Color[][] grid = new Color[20][20];

	int level  = 1;

	Piece current = Piece.create(0,1),next = Piece.create(1,1);
	int score = 0;
	HashSet<Integer> down = new HashSet<Integer>();
	ArrayList<Bomb> bombs = new ArrayList<Bomb>();
	ArrayList<Explosion> explosions = new ArrayList<Explosion>();

	boolean die = false;

	public Tetris()
	{
		for(int i = 0; i < 20; i ++)
			for(int j = 0; j < 20; j++)
				grid[i][j] = Color.WHITE;

		new Thread(this).start();

		if(current instanceof Bomb)
			bombs.add((Bomb)current);

		addKeyListener(this);
	}

	public void keyPressed(KeyEvent evt)
	{
		down.add(evt.getKeyCode());


		if(evt.getKeyCode() == KeyEvent.VK_UP)
			tryRotate();
	}
	public void keyReleased(KeyEvent evt)
	{
		down.remove(evt.getKeyCode());
	}
	public void keyTyped(KeyEvent evt)
	{
	}

	public void paint(Graphics g)
	{
		if(getBufferStrategy() == null)
			createBufferStrategy(2);

		g = getBufferStrategy().getDrawGraphics();

		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0,0,SCREEN.width,SCREEN.height);

		for(int i = 0; i < 20; i++)
			for(int j = 0; j < 20; j++)
			{
				g.setColor(grid[i][j]);
				g.fillRect((SCREEN.width-SCREEN.height)/2 + i * SCREEN.height/20,
					j * SCREEN.height/20,SCREEN.height/20 + 1,SCREEN.height/20 + 1);
				g.setColor(Color.BLACK);
				g.drawRect((SCREEN.width-SCREEN.height)/2 + i * SCREEN.height/20,
					j * SCREEN.height/20,SCREEN.height/20 + 1,SCREEN.height/20 + 1);
			}
		//g.setColor(Color.DARK_GRAY);
		//g.fillRect((int)((SCREEN.width-SCREEN.height)/2 + (current.x) * SCREEN.height/20),
		//		(int)((current.y) * SCREEN.height/20),current.w*SCREEN.height/20 + 1,
		//			current.h*SCREEN.height/20 + 1);

		for(Bomb b: bombs)
		{
			g.setColor(b.color);
			for(Point p : b.points)
			{
				g.fillRect((int)((SCREEN.width-SCREEN.height)/2 +
						(p.x+b.x) * SCREEN.height/20),
					(int)((p.y+b.y) * SCREEN.height/20),SCREEN.height/20 +
						1,SCREEN.height/20 + 1);
			}
		}

		g.setColor(current.color);

		for(Point p : current.points)
		{
			g.fillRect((int)((SCREEN.width-SCREEN.height)/2 + (p.x+current.x) * SCREEN.height/20),
				(int)((p.y+current.y) * SCREEN.height/20),SCREEN.height/20 + 1,SCREEN.height/20 + 1);
		}

		g.setColor(Color.BLACK);
		for(Bomb b: bombs)
		{
			if(b instanceof Bomb.TimeBomb)
			{
				g.drawString( ((Bomb.TimeBomb)b).remaining+"",
					(int)((SCREEN.width-SCREEN.height)/2 + (b.x) * SCREEN.height/20),
					(int)((b.y) * SCREEN.height/20)+20);
			}
			if(b instanceof Bomb.PressureBomb)
			{
				//System.out.println(((Bomb.PressureBomb)b).pressure+": is the pressure");
				g.drawString( ((Bomb.PressureBomb)b).pressure+" / "+((Bomb.PressureBomb)b).maxPressure,
					(int)((SCREEN.width-SCREEN.height)/2 + (b.x) * SCREEN.height/20),
					(int)((b.y) * SCREEN.height/20)+20);
			}
		}

		g.setColor(next.color);

		for(Point p : next.points)
		{
			g.fillRect((int)((SCREEN.width-SCREEN.height)/2 + (p.x+41) * SCREEN.height/40),
				(int)((p.y+10) * SCREEN.height/40),SCREEN.height/40 + 1,SCREEN.height/40 + 1);
		}


		for(Explosion e: explosions)
		{
			g.drawImage(e.getImage(),e.posx-e.r,e.posy-e.r,this);
		}

		g.setFont(new Font("SANS_SERIF",Font.BOLD,50));
		g.setColor(Color.BLACK);
		g.drawString(""+score,0,400);
		g.drawString(""+level,0,600);


		getBufferStrategy().show();
	}

	public void tryRotate()
	{
		current.rotate();

		for(Point p : current.points)
			if((int)(current.x-1+p.x)<0|| (int)(current.x-1+p.x)>=20||
				(int)(current.y+p.y)<0||(int)(current.y+p.y)>=20||
				grid[(int)(current.x-1+p.x)][(int)(current.y+p.y)] != Color.WHITE)
			{
				current.antirotate();
				return;
			}
	}

	public boolean wouldCollideLeft()
	{
		if(current.x==0)
			return true;

		boolean resting = false;
		for(Point p : current.points)
				resting = resting  ||
					(grid[(int)(current.x-1+p.x)][(int)(current.y+p.y)] != Color.WHITE);

		return resting;
	}

	public boolean wouldCollideRight()
	{
		if(current.x + current.w >= 20)
			return true;

		boolean resting = false;
		for(Point p : current.points)
				resting = resting  ||
					(grid[(int)(current.x+p.x+1)][(int)(current.y+p.y)] != Color.WHITE);

		return resting;
	}

	public boolean isResting()
	{
		if(current.y + current.h >= 20)
			return true;

		boolean resting = false;
		for(Point p : current.points)
				resting = resting  ||
					(grid[(int)(current.x+p.x)][(int)(current.y+p.y+1)] != Color.WHITE);

		return resting;
	}

	public boolean hasLost()
	{
		for(int i =0; i < 20; i++)
			if(grid[i][0] != Color.WHITE)
				return true;

		return false;
	}

	public int clearRows()
	{
		int num = 0;
		for(int j = 0; j < 20; j++)
		{
			boolean filled = true;
			for(int i = 0; i < 20; i++)
			{
				if(grid[i][j] == Color.WHITE)
					filled = false;
			}

			if(filled)
			{
				num++;

				for(int i = 0; i < 20; i++)
					grid[i][j] = Color.WHITE;

				for(int y = j; y >= 0; y--)
					for(int i = 0; i < 20; i++)
						if(y==0)
							grid[i][0] = Color.WHITE;
						else
							grid[i][y] = grid[i][y-1];
			}
		}
		return num;
	}

	public void update()
	{
		for(int i = 0; i < bombs.size(); i++)
		{
			Bomb b = bombs.get(i);
			b.update();
			if(b.willExplode(grid))
			{
				bombs.remove(b);
				b.explose(grid, explosions);

				if(b == current)
				{
					current = next;
					next = Piece.create((int)(Math.random()*Piece.NUM_PIECES[level-1]),level);
					if(current instanceof Bomb)
						bombs.add((Bomb)current);
				}
			}
		}

		if(hasLost())
		{
			restart();
		}

		int cleared = clearRows();
		die = die || cleared>0;

		score += Math.pow(2,cleared)-1;

		if(isResting())
		{
			for(Point p : current.points)
				grid[(int)(p.x+current.x)][(int)(p.y+current.y)] = current.color;

			if(die)
				current = Piece.createEvil();
			else
			{
				current = next;
				next = Piece.create((int)(Math.random()*Piece.NUM_PIECES[level-1]),level);

				if(current instanceof Bomb)
					bombs.add((Bomb)current);
			}

			die = false;

			cleared = 0;


			current.x = 10;
			current.y = 0;
		}
		else
			current.y += 1;
	}

	public void restart()
	{
		for(int i = 0; i < 20; i++)
			for(int j = 0; j < 20; j++)
				grid[i][j] = Color.WHITE;

		score = 0;

		bombs.clear();
		explosions.clear();
	}

	public void run()
	{
		int counter = 0;
		long last = System.currentTimeMillis();

		while(true)
		{
			try
			{
				Thread.sleep(1);
			}catch(Exception e){e.printStackTrace();}

			if(System.currentTimeMillis()-last > 0)
			{
				if(counter%6==0)
					for(int i : down)
					{

						if(i == KeyEvent.VK_DOWN && !isResting())
							current.y += 1;
						if(i == KeyEvent.VK_RIGHT && !wouldCollideRight())
							current.x += 1;
						if(i == KeyEvent.VK_LEFT && !wouldCollideLeft())
							current.x -= 1;

						current.x = Math.max(Math.min(20-current.w,current.x),0);
						current.y = Math.max(Math.min(20-current.h,current.y),0);


					}

				last = System.currentTimeMillis();


				if(counter++% (1000/(2+level))==0)
					update();



				//System.out.println(explosions.size());

				if(counter % 1==0)
					for(int i =0; i < explosions.size(); i++)
					{
						Explosion e = explosions.get(i);
						e.update();

						if(e.done())
							explosions.remove(i);
					}
			}

			repaint();
		}
	}

	public static void main(String[] args)
	{
		Tetris t = new Tetris();
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		t.setUndecorated(true);
		t.setSize(SCREEN.width, SCREEN.height);
		t.setVisible(true);
	}
}