import java.awt.*;
import java.awt.image.*;

class Explosion
{
	int posx, posy;
	int size, r = 1000, psize = 20;
	int[] x, y, vx, vy;
	Color color = Color.LIGHT_GRAY;

	public Explosion(int particles, int velocity)
	{
		this.size = particles;
		x = new int[size];
		y = new int[size];
		vx = new int[size];
		vy = new int[size];

		for(int i = 0; i < size; i++)
		{
			x[i] = 0;
			y[i] = 0;
			double theta = Math.random()*2*Math.PI;
			vx[i] = (int)(Math.random()*velocity*Math.sin(theta));
			vy[i] = (int)(Math.random()*velocity*Math.cos(theta));
		}
	}

	public boolean done()
	{
		for(int asdf : y)
			if(Math.abs(asdf) < r)
				return false;

		return true;

	}

	public void update()
	{
		for(int i = 0; i < size; i++)
		{
			x[i] += vx[i];
			y[i] += vy[i];

			vy[i]+= 1;
		}
	}

	public BufferedImage getImage()
	{
		BufferedImage image = new BufferedImage(2*r, 2*r, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = image.createGraphics();

		for(int i = 0; i < size; i++)
		{
			g.setColor(color);
			g.fillRect(r+x[i]-psize/2,r+y[i]-psize/2,psize,psize);
		}

		g.dispose();

		return image;
	}
		public static void main(String args[])
		{
			Tetris.main(args);
	}
}