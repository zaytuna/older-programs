import java.awt.*;

abstract class Bomb extends Piece
{
	int blastRadius = 2;
	Color begin;

	public Bomb(Color c)
	{
		super();
		this.begin = c;
		color = c;
	}

	public abstract boolean willExplode(Color[][] grid);
	public void update()
	{
	}

	public static class TimeBomb extends Bomb
	{
		public int duration;
		public int remaining;

		public TimeBomb(int duration, Color c)
		{
			super(c);
			this.duration = duration;
			remaining = duration;
		}

		public boolean willExplode(Color[][] grid)
		{

			return remaining <= 0;
		}

		public void update()
		{
			remaining--;
			color = new Color(
				Math.max(Math.min(begin.getRed()+5*(duration-remaining),255),0),
				Math.max(Math.min(begin.getGreen()*15/(duration-remaining),255),0),
				Math.max(Math.min(begin.getBlue()-4*(duration-remaining),255),0));
		}
	}

	public static class PressureBomb extends Bomb
	{
		int maxPressure, pressure;

		public PressureBomb(int p, Color c)
		{
			super(c);
			maxPressure = p;
			blastRadius = p;
		}

		public boolean willExplode(Color[][] grid)
		{
			pressure = 0;

			for(int i = y; i >= 0; i--)
			{
				if(grid[x][i]== Color.WHITE)
					break;
				pressure++;
			}


			if(pressure>=maxPressure)
				return true;

			return false;
		}

		public void update()
		{
			if(pressure == 0)
				color = new Color(100,0,200,50);
			else
				color = new Color(
					Math.max(Math.min(begin.getRed()-30*(pressure+1),255),0),
					Math.max(Math.min(begin.getGreen()*1*(pressure+1),255),0),
					Math.max(Math.min(begin.getBlue()+30*(pressure+1),255),0));
		}
	}

	public void explose(Color[][] grid, java.util.List<Explosion> exp)
	{
		Explosion e = new Explosion(50*blastRadius, 50);
		e.posx = (Tetris.SCREEN.width-Tetris.SCREEN.height)/2 + x*Tetris.SCREEN.height/20;
		e.posy = y*Tetris.SCREEN.height/20;

		exp.add(e);

		for(int i = -blastRadius; i < blastRadius; i++)
			for(int j = -blastRadius; j < blastRadius; j++)
			{
				try{
					grid[i+x][j+y] = Color.WHITE;
				}catch(Exception eajfasdahsdasdhfkjashalsdfeuwibvsdsdf){}
			}
	}

	public static void main(String args[])
	{
		Tetris.main(args);
	}
}