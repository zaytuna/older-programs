import java.awt.Color;
import java.awt.Point;

public class Piece
{
	public static final int[] NUM_PIECES = {9};

	public int w, h;
	public Point[] points;//this is a comment
	public int x, y;
	public Color color;
	public int special = 0;	//0 = nothing
							//1 = pressure-bomb
							//2 = time-bomb
							//3 = liquid;

	protected Piece(){}

	public void rotate()
	{
		set(h,w);
		int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;

		for(int i = 0; i< points.length; i++)
		{
			points[i] = new Point(points[i].y, -points[i].x);

			if(points[i].x < minX)
				minX = points[i].x;
			if(points[i].y < minY)
				minY = points[i].y;
		}

		for(int i = 0; i< points.length; i++)
			points[i] = new Point(points[i].x - minX, points[i].y - minY);
	}

	public void antirotate()
	{
		set(h,w);
		int minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;

		for(int i = 0; i< points.length; i++)
		{
			points[i] = new Point(-points[i].y, points[i].x);

			if(points[i].x < minX)
				minX = points[i].x;
			if(points[i].y < minY)
				minY = points[i].y;
		}

		for(int i = 0; i< points.length; i++)
			points[i] = new Point(points[i].x - minX, points[i].y - minY);
	}

	protected void set(int w, int h)
	{
		this.w = w;
		this.h = h;
	}

	public static Piece createEvil()
	{
		Piece p = new Piece();
		p.points = new Point[26];
		//D
		p.points[0] = new Point(0,0);
		p.points[1] = new Point(0,1);
		p.points[2] = new Point(0,2);
		p.points[3] = new Point(0,3);
		p.points[4] = new Point(0,4);
		p.points[5] = new Point(1,0);
		p.points[6] = new Point(2,1);
		p.points[7] = new Point(2,2);
		p.points[8] = new Point(2,3);
		p.points[9] = new Point(1,4);

		//I
		p.points[10] = new Point(4,0);
		p.points[11] = new Point(4,1);
		p.points[12] = new Point(4,2);
		p.points[13] = new Point(4,3);
		p.points[14] = new Point(4,4);

		//E
		p.points[15] = new Point(6,0);
		p.points[16] = new Point(6,1);
		p.points[17] = new Point(6,2);
		p.points[18] = new Point(6,3);
		p.points[19] = new Point(6,4);

		p.points[20] = new Point(7,0);
		p.points[21] = new Point(8,0);
		p.points[22] = new Point(7,2);
		p.points[23] = new Point(8,2);
		p.points[24] = new Point(7,4);
		p.points[25] = new Point(8,4);

		p.color = Color.RED;
		p.special = 3;

		p.set(9,5);

		return p;
	}

	public static Piece create(int type, int level)
	{
		Piece p = new Piece();

		switch(level)
		{
			default:
				switch(type)
				{
					case 0:
						p.points = new Point[4];

						p.points[0] = new Point(0,0);
						p.points[1] = new Point(0,1);
						p.points[2] = new Point(0,2);
						p.points[3] = new Point(0,3);
						p.color = new Color(0,0,80);

						p.set(1,4);

						return p;

					case 1:
						p.points = new Point[4];
						p.points[0] = new Point(0,0);
						p.points[1] = new Point(1,1);
						p.points[2] = new Point(0,1);
						p.points[3] = new Point(1,2);
						p.color = new Color(0,0,120);

						p.set(2,3);

						return p;

					case 2:
						p.points = new Point[4];

						p.points[0] = new Point(0,0);
						p.points[1] = new Point(0,1);
						p.points[2] = new Point(0,2);
						p.points[3] = new Point(1,2);
						p.color = new Color(0,0,160);

						p.set(2,3);

						return p;

					case 3:
						p.points = new Point[4];

						p.points[0] = new Point(1,0);
						p.points[1] = new Point(1,1);
						p.points[2] = new Point(1,2);
						p.points[3] = new Point(0,2);
						p.color = new Color(0,0,190);

						p.set(2,3);

						return p;

					case 4:
						p.points = new Point[4];
						p.points[0] = new Point(1,0);
						p.points[1] = new Point(1,1);
						p.points[2] = new Point(0,1);
						p.points[3] = new Point(0,2);
						p.color = new Color(0,0,220);

						p.set(2,3);

						return p;

					case 5:
						p.points = new Point[4];
						p.points[0] = new Point(1,0);
						p.points[1] = new Point(1,1);
						p.points[2] = new Point(0,1);
						p.points[3] = new Point(2,1);
						p.color = new Color(0,0,255);

						p.set(3,2);

						return p;

					case 6:
						p = new Bomb.TimeBomb(30, Color.ORANGE);
						p.points = new Point[1];
						p.points[0] = new Point(0,0);
						p.set(1,1);
						return p;

					case 7:
						p = new Bomb.PressureBomb(5, Color.YELLOW);
						p.points = new Point[1];
						p.points[0] = new Point(0,0);
						p.set(1,1);
						return p;

					default:
						p.points = new Point[4];

						p.points[0] = new Point(0,0);
						p.points[1] = new Point(1,0);
						p.points[2] = new Point(0,1);
						p.points[3] = new Point(1,1);
						p.color = new Color(50,50,255);

						p.set(2,2);

						return p;
				}
			}
	}
		public static void main(String args[])
		{
			Tetris.main(args);
	}
}