import javax.swing.*;
import java.awt.*;

public class Chaos
{
	public static void main(String[] args) throws Exception
	{
		Robot r = new Robot();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

		int counter = 0;

		while(true)
		{
			JFrame j = new JFrame();
			j.setSize((int)(Math.random()*500)+100,(int)(Math.random()*100)+100);
			j.setUndecorated(true);
			j.setLocation((int)(Math.random()*screen.width),(int)(Math.random()*screen.height));
			j.getContentPane().setBackground(new Color((int)(Math.random()*255),(int)(Math.random()*255),(int)(Math.random()*255)));
			j.setVisible(true);
			j.setAlwaysOnTop(true);


			r.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
			r.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
			r.mousePress(java.awt.event.InputEvent.BUTTON1_MASK);
			r.mouseMove((int)(Math.random()*screen.width),(int)(Math.random()*screen.height));
			r.mouseRelease(java.awt.event.InputEvent.BUTTON1_MASK);
		}
	}
}