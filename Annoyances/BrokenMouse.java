
import java.awt.*;

public class BrokenMouse
{
	public static void main(String[] args) throws Exception
	{
		Robot r = new Robot();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

		int counter = 0;
		double direction = 0;

		Point mouse = MouseInfo.getPointerInfo().getLocation();
		Point mouse2 = MouseInfo.getPointerInfo().getLocation();
		double amt = 0;


		while(true)
		{
			if(counter++%1000==0)
				direction = Math.random()*Math.PI*2;
		//	direction += .01;

			mouse2 = MouseInfo.getPointerInfo().getLocation();
			amt = amt/2 + (Math.abs(mouse2.x-mouse.x)+Math.abs(mouse.y-mouse2.y))/3;

			r.mouseMove(mouse2.x+(int)(Math.cos(direction)*amt),mouse2.y+(int)(Math.sin(direction)*amt));

			mouse = new Point(mouse2.x,mouse2.y);
		}
	}
}