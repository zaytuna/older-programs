
import java.awt.*;

public class HelpMe
{
	public static void main(String[] args) throws Exception
	{
		Robot r = new Robot();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

		int counter = 0;

		while(true)
		{
			for(int i = 0; i < 10; i++)
			{
				r.keyPress(java.awt.event.KeyEvent.VK_F1);
				r.keyRelease(java.awt.event.KeyEvent.VK_F1);
			}

			r.keyPress(java.awt.event.KeyEvent.VK_ALT);
			r.keyPress(java.awt.event.KeyEvent.VK_TAB);
			r.keyRelease(java.awt.event.KeyEvent.VK_ALT);
			r.keyRelease(java.awt.event.KeyEvent.VK_TAB);

			r.keyPress(java.awt.event.KeyEvent.VK_WINDOWS);
			r.keyPress(java.awt.event.KeyEvent.VK_D);
			r.keyRelease(java.awt.event.KeyEvent.VK_WINDOWS);
			r.keyRelease(java.awt.event.KeyEvent.VK_D);

			Point mouse = MouseInfo.getPointerInfo().getLocation();

			r.mouseMove(mouse.x+(int)(Math.random()*100-50),mouse.y+(int)(Math.random()*100-50));
		}
	}
}