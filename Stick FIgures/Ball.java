import java.awt.Point;
import java.awt.Color;
import java.io.Serializable;

class Ball implements Serializable
{
	private int diameter = 20;
	private Point center = new Point(10,10);
	private Point upperLeft = new Point(0,0);
	static int count = 0;
	Color shade = Color.red;

	Ball(int d,Point c)
	{
		this(d,c,Color.blue);
	}
	Ball(int d,Point c, Color s)
	{
		center = new Point(c.x,c.y);
		diameter = d;
		upperLeft = new Point(c.x-(d/2),c.y-(d/2));
		count++;
		shade = s;
	}
	public String toString()
	{
		return "�("+center.x+","+center.y+")&"+diameter;
	}
	public Point getCenter()
	{
		return center;
	}
	public Point getUpperLeft()
	{
		return upperLeft;
	}
	public int getCount()
	{
		return count;
	}
	public void setColor(Color newC)
	{
		shade = newC;
	}
	public Color getColor()
	{
		return shade;
	}
	public void setCenter(Point p)
	{
		center = new Point(p.x,p.y);
		upperLeft = new Point(p.x-(diameter/2),p.y-(diameter/2));
	}
	public void setDiameter(int newD)
	{
		diameter = newD;
		upperLeft = new Point(center.x-(newD/2),center.y-(newD/2));
	}
	public int getDiameter()
	{
		return diameter;
	}
	public void setUpperLeft(Point p)
	{
		upperLeft = new Point(p.x,p.y);
	}
}