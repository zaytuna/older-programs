import java.awt.*;
import java.util.*;
import java.io.*;

class BallList extends ArrayList<Ball> implements Serializable
{
	Ball ball;

	BallList()
	{
	}
	BallList(Ball b)
	{
		ball = new Ball(b.getDiameter(),b.getCenter(),b.getColor());
		add(b);
	}
	BallList(Ball[] bl)
	{
		for(int i = 0; i < bl.length; i++)
		{
			ball = new Ball(bl[i].getDiameter(),bl[i].getCenter(),bl[i].getColor());
			add(new Ball(bl[i].getDiameter(),bl[i].getCenter(),bl[i].getColor()));
		}
	}
	BallList(ArrayList<Ball> ab)
	{
		for(int i = 0; i < ab.size(); i++)
		{
			ball = new Ball(ab.get(i).getDiameter(),ab.get(i).getCenter(),ab.get(i).getColor());
			add(new Ball(ab.get(i).getDiameter(),ab.get(i).getCenter(),ab.get(i).getColor()));
		}
	}

	public String toString()
	{
		String s = "";

		for(int i = 0; i < size(); i++)
			s += get(i).toString()+" ";
		s.trim();
		return s;
	}
}