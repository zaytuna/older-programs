import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

class SliderDialog extends JDialog implements ActionListener,ChangeListener
{
	private JLabel lbl = new JLabel("");
	private JSlider slider;
	private GradientButton confirm = new GradientButton(new Color(0,100,0),new Color(0,255,0));
	private int start = 0;
	private int end = 100;
	private JLabel lbl2 = new JLabel("");
	private String message = "";
	private Color color = null;
	private int pos = 0;
	private int answer = 0;

	SliderDialog(String m,int starting,int ending,int positioning,Color c)
	{
		centerFrame(350,120);
		setLayout(null);
		confirm.setText("Confirm");
		color = c;
		start = starting;
		end = ending;
		pos = positioning;
		slider = new JSlider(JSlider.HORIZONTAL,start,end,pos);
		slider.setForeground(c);
		message = m;
		lbl.setText(m);
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		//addMouseListener(new Mouse());
		lbl2.setText(""+pos+"/"+end);
		confirm.addActionListener(this);
		slider.addChangeListener(this);

		lbl.setBounds(20,5,180,25);
		add(lbl);
		lbl2.setBounds(200,5,100,20);
		add(lbl2);
		slider.setBounds(20,30,300,25);
		add(slider);
		confirm.setBounds(150,60,120,25);
		add(confirm);
		setVisible(true);
	}
	public void stateChanged(ChangeEvent evt)
	{
		pos = slider.getValue();
		lbl2.setText(""+pos+"/"+end);
	}
	public void actionPerformed(ActionEvent evt)
	{
		try
		{
			answer=slider.getValue();
			StickFigurePanel.doneJSlider(answer);
			setVisible(false);
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(null,"ERROR: "+e,"",0);
			try
			{
				Thread.sleep(3000);
			}
			catch(Exception er){}
		}
	}
	public void centerFrame(int frameWidth, int frameHeight)
	{
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width-frameWidth)/2;
		int yUpperLeftCorner = (screen.height-frameHeight)/2;

		setBounds(xUpperLeftCorner,yUpperLeftCorner,frameWidth,frameHeight);
	}
	private class Mouse extends MouseAdapter implements MouseListener
	{
		public void mouseExited(MouseEvent evt)
		{
			//System.out.println(evt.getPoint());
		}
	}
}