import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.StringTokenizer;
import java.util.ArrayList;

class StickFigurePanel extends JPanel
{
	static String doingWhat = "";
	static Point[] point = new Point[12];
	static Point outside;
	static int id = 0;
	static int diameter = 6;
	static int width = 1;
	static Color color = Color.black;
	static BallList list = new BallList();
	static StickFigure stick;
	boolean onTop = false;

	public Point[] predefinedPoints = {new Point(150,350),new Point(250,350), new Point(150,300), new Point(250,300),
		new Point(200,250),new Point(150,225), new Point(160,175), new Point(200,140),new Point(240,120),
		new Point(300,75), new Point(200,120), new Point(200,75)};

	StickFigurePanel()
	{
		setBackground(Color.white);
		doingWhat = "DRAWING";
		drawFigure(predefinedPoints);
		addMouseListener(new MouseSteak());
		addMouseMotionListener(new MouseSteak());
	}

	public void paintComponent(Graphics g)
	{
		Graphics2D g2d = (Graphics2D)g;
		super.paintComponent(g2d);

		if(doingWhat.equals("DRAWING"))
		{
			try
			{
				if(stick == null)
					stick = new StickFigure(point);
				if(stick.isOnTop()||StickAnimator.top)
				{
					for(int i = 0; i < list.size(); i++)
					{
						g2d.setColor(list.get(i).getColor());
						g2d.fillOval(list.get(i).getUpperLeft().x,list.get(i).getUpperLeft().y,list.get(i).getDiameter(),
							list.get(i).getDiameter());
					}
				}
			}
			catch(Exception e){JOptionPane.showMessageDialog(StickFigurePanel.this,"ERROR\n"+e,"EXCEPTION!!",0);}
			g2d.setColor(color);
			g2d.setStroke(new BasicStroke(width,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
			g2d.drawLine(point[0].x,point[0].y,point[2].x,point[2].y);
			g2d.drawLine(point[1].x,point[1].y,point[3].x,point[3].y);
			g2d.drawLine(point[2].x,point[2].y,point[4].x,point[4].y);
			g2d.drawLine(point[3].x,point[3].y,point[4].x,point[4].y);
			g2d.drawLine(point[7].x,point[7].y,point[4].x,point[4].y);
			g2d.drawLine(point[7].x,point[7].y,point[6].x,point[6].y);
			g2d.drawLine(point[7].x,point[7].y,point[8].x,point[8].y);
			g2d.drawLine(point[7].x,point[7].y,point[10].x,point[10].y);
			g2d.drawLine(point[5].x,point[5].y,point[6].x,point[6].y);
			g2d.drawLine(point[8].x,point[8].y,point[9].x,point[9].y);
			g2d.drawOval(point[11].x-(Math.abs(point[11].y-point[10].y)),point[11].y-Math.abs(point[11].y-point[10].y),
				Math.abs(point[11].y-point[10].y)*2,Math.abs(point[11].y-point[10].y)*2);

			g2d.setColor(Color.green);
			for(int i = 0; i < 12; i++)
				g2d.fillOval((point[i].x-(diameter/2)),(point[i].y-(diameter/2)),diameter,diameter);

			if(!stick.isOnTop())
			{
				for(int i = 0; i < list.size(); i++)
				{
					g2d.setColor(list.get(i).getColor());
					g2d.fillOval(list.get(i).getUpperLeft().x,list.get(i).getUpperLeft().y,list.get(i).getDiameter(),
						list.get(i).getDiameter());
				}
			}

			g2d.setStroke(new BasicStroke(1,1,1,1));
		}
	}
	public void setDiameter(int newD)
	{
		diameter = newD;
		repaint();
	}
	public void setWidth(int newW)
	{
		width = newW;
		repaint();
	}
	public void drawFigure(StickFigure sf)
	{
		doingWhat = "DRAWING";
		stick = sf;
		Point[] points = sf.getPoints();

		for(int x = 0; x < 12; x++)
		{
			point[x].x = points[x].x;
			point[x].y = points[x].y;
		}
		list.clear();
		repaint();
	}
	public void drawFigure(StickFigure sf, BallList bls)
	{
		list.clear();
		doingWhat = "DRAWING";
		Point[] points = sf.getPoints();

		for(int x = 0; x < 12; x++)
		{
			point[x].x = points[x].x;
			point[x].y = points[x].y;
		}
		for(int i = 0; i < bls.size(); i++)
		{
			list.add(bls.get(i));
		}
		repaint();
	}
	public void addBall(Ball b)
	{
		doingWhat = "DRAWING";
		list.add(b);
		repaint();
		System.out.println(list.size());
	}
	public BallList getBalls()
	{
		return list;
	}
	public Point[] getPoints()
	{
		return point;
	}
	public void drawFigure(Point[] p)
	{
		try
		{
			doingWhat = "DRAWING";

			for(int b = 0; b < 12; b++)
			{
				point[b] = new Point(p[b].x,p[b].y);
			}
			repaint();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,e+"\n"+e.getMessage()+"\n"+e.getStackTrace());
		}
	}

	private class MouseSteak extends MouseAdapter implements MouseListener, MouseMotionListener
	{
		public void mousePressed(MouseEvent evt)
		{
			boolean found = false;
			for(int u = 0; u < 12; u++)
			{
				if((evt.getPoint().x > (point[u].x-(diameter/2)))&&
					(evt.getPoint().x < (point[u].x+(diameter/2)))&&
					(evt.getPoint().y > (point[u].y-(diameter/2)))&&
					(evt.getPoint().y < (point[u].y+(diameter/2))))
				{
					id = u;
					found = true;
					break;
				}
			}
			for(int i=0; i<list.size();i++)
			{
				if((evt.getPoint().x > (list.get(i).getCenter().x-(list.get(i).getDiameter()/2)))&&
					(evt.getPoint().x < (list.get(i).getCenter().x+(list.get(i).getDiameter()/2)))&&
					(evt.getPoint().y > (list.get(i).getCenter().y-(list.get(i).getDiameter()/2)))&&
					(evt.getPoint().y < (list.get(i).getCenter().y+(list.get(i).getDiameter()/2))))
				{
					id = (14+i);
					found = true;
					break;
				}
			}

			if(!found)
				id = 13;
			else
				System.out.println("ID == "+id);
		}
		public void mouseClicked(MouseEvent evt)
		{
			boolean found = false;
			for(int i=0; i<list.size();i++)
			{
				if((evt.getPoint().x > (list.get(i).getCenter().x-(list.get(i).getDiameter()/2)))&&
					(evt.getPoint().x < (list.get(i).getCenter().x+(list.get(i).getDiameter()/2)))&&
					(evt.getPoint().y > (list.get(i).getCenter().y-(list.get(i).getDiameter()/2)))&&
					(evt.getPoint().y < (list.get(i).getCenter().y+(list.get(i).getDiameter()/2))))
				{
					id = (14+i);
					found = true;
					break;
				}
			}

			if(!found)
				return;
			else
			{
				if(evt.getClickCount() == 2)
				{
					Color newColor = JColorChooser.showDialog(StickFigurePanel.this,"Choose a color for the ball"
						,list.get((id-14)).getColor());
					if(newColor == null)
						newColor = list.get((id-14)).getColor();
					list.get((id-14)).setColor(newColor);
				}
				else if(evt.isMetaDown() == true)
				{
					int ans = JOptionPane.showConfirmDialog(StickFigurePanel.this,
						"Are you sure you wish to delete this ball?");
					if(ans == 0)
						list.remove(id-14);
					repaint();
				}
				else if(evt.isAltDown())
				{
					try
					{
						new SliderDialog("Choose the diameter",2,300,list.get(id-14).getDiameter(),Color.blue);
					}
					catch(Exception e)
					{
						JOptionPane.showMessageDialog(null,e,"",0);
					}
				}

			}
		}
		public void mouseMoved(MouseEvent evt){}
		public void mouseDragged(MouseEvent evt)
		{
			if(id != 13&& evt.getPoint().x >0&& evt.getPoint().y >0&& evt.getPoint().x < getWidth()&& evt.getPoint().y < getHeight())
			{
				if(id<13)
				{
					point[id] = new Point(evt.getPoint().x,evt.getPoint().y);
					drawFigure(point);
				}
				else
				{
					try
					{
						Ball ball = list.get((id-14));
						ball.setCenter(new Point(evt.getPoint().x,evt.getPoint().y));
						list.remove((id-14));
						list.add((id-14),ball);
					}
					catch(Exception e)
					{
						e.printStackTrace();
						JOptionPane.showMessageDialog(null,e+"\n\n"+e.getMessage(),"EXCEPTION == ",0);
					}

					repaint();
				}
			}
			else
			{
				outside = new Point(evt.getPoint().x,evt.getPoint().y);
			}
		}
	}
	public static void doneJSlider(int s)
	{
		list.get(id-14).setDiameter(s);
	}
	public void clrBalls()
	{
		list.clear();
	}
	public void animate()
	{
		setBackground(Color.black);
		color = Color.white;
	}
	public void deAnimate()
	{
		setBackground(Color.white);
		color = Color.black;
	}
	public static StickFigure toFigure(String line)
	{
		Point[] newPtsBlah = StickFigurePanel.toPoints(line);
		if(newPtsBlah == null)
		{
			JOptionPane.showMessageDialog(null,"BAZZOKA!D!!D\n\n(bad)");
			return new StickFigure(new Point[12]);
		}
		return new StickFigure(newPtsBlah);
	}
	public static Point[] toPoints(String line)
	{
		try
		{
			// line in format: (4,5) (x,y) (b,c) (aa,bbbb)
			Point pts[] = new Point[12];
			StringTokenizer breaker = new StringTokenizer(line," ");
			int countOfTokens = breaker.countTokens();

			for(int i = 0; i < countOfTokens; i++)
			{
				String next = breaker.nextToken();
				if(next.charAt(0) == '�')
					break;
				//System.out.println("Itteration: <"+i+"/"+countOfTokens+">\tThe Token: "+next);
				next.trim();
				StringBuilder build = new StringBuilder(next);
				build = build.deleteCharAt(0);
				//System.out.println("Itteration: <"+i+"/"+countOfTokens+
				//	">\tAfter parenthisis 1 deleted: "+build.toString());
				// e.g. "90,1344)"
				build = build.deleteCharAt(build.length()-1);
				//System.out.println("Itteration: <"+i+"/"+countOfTokens+
				//	">\tAfter parenthisis 2 deleted: "+build.toString());
				// e.g. "90,1344"
				next = build.toString();
				int xValue = Integer.parseInt(next.substring(0,next.indexOf(",")));
				int yValue = Integer.parseInt(next.substring((next.indexOf(",")+1),next.length()));
				//System.out.println("Itteration: <"+i+"/"+countOfTokens+
				//	">\tPoint: ("+xValue+","+yValue+")");
				pts[i] = new Point(xValue,yValue);
			}
			//System.out.println("\n\n");
			return pts;
		}
		catch(Exception e){JOptionPane.showMessageDialog(null,"Problem: "+e);return null;}
	}
	public static BallList toBallList(String line)
	{
		try
		{
			// line in format: �(4,5)&5 �(x,y)&z �(b,c)&dd �(aa,bbbb)&cccc
			BallList b = new BallList();
			StringTokenizer breaker = new StringTokenizer(line," ");
			int countOfTokens = breaker.countTokens();

			for(int i = 0; i < countOfTokens; i++)
			{
				String next = breaker.nextToken();
				next.trim();

				if(next.charAt(0) == '�')
				{
					//next = "�(90,1344)&20"
					StringBuilder build = new StringBuilder(next);
					build = build.deleteCharAt(0);
					//next = "(90,1344)&20"
					build = build.deleteCharAt(0);
					//next = "90,1344)&20"
					build = build.deleteCharAt(build.indexOf("&")-1);
					//next = "90,1344&20"
					next = build.toString();
					int xValue = Integer.parseInt(next.substring(0,next.indexOf(",")));
					int yValue = Integer.parseInt(next.substring((next.indexOf(",")+1),next.indexOf("&")));
					int dmeter = Integer.parseInt(next.substring((next.indexOf("&")+1),next.length()));
					b.add(new Ball(dmeter,new Point(xValue,yValue)));
				}
			}
			return b;
		}
		catch(Exception e){JOptionPane.showMessageDialog(null,"Problem: "+e);return null;}
	}
}