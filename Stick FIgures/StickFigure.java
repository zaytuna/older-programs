import java.awt.Point;
import java.io.Serializable;

class StickFigure implements Serializable
{
	public Point[] points = new Point[12];
	// 2 feet, 2 knees, pelvis joint, chest joint, 2 elbows, 2 hands, neck, head
	boolean onTop = false;

	StickFigure(Point[] p)
	{
		for(int i = 0; i < 12; i++)
		{
			points[i] = new Point(p[i].x,p[i].y);
		}
	}
	public void setPoints(Point[] p)
	{
		for(int i = 0; i < 12; i++)
		{
			points[i] = new Point(p[i].x,p[i].y);
		}
	}
	public Point[] getPoints()
	{
		return points;
	}
	public String toString()
	{
		String temp = "";
		for(Point p: points)
			temp += "("+p.x+","+p.y+") ";

		return temp;
	}
	public void setOnTop(boolean b)
	{
		onTop = b;
	}
	public boolean isOnTop()
	{
		return onTop;
	}
}