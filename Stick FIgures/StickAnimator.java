import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.border.*;
import java.text.*;
import java.util.ArrayList;
import java.io.*;
import javax.swing.event.*;

class StickAnimator extends JFrame implements ActionListener, ChangeListener
{
	Container pain = getContentPane();
	JPanel buttons = new JPanel();
	StickFigurePanel drawFigure = new StickFigurePanel();
	JButton clr = new JButton("Delete all frames");
	JButton animate = new JButton("Animate");
	JButton pause = new JButton("Pause");
	JButton stop = new JButton("Stop");
	JButton back = new JButton("Back");
	JButton newFrame = new JButton("Add Frame");
	JButton foreward = new JButton("Next");
	JLabel ind = new JLabel("  On frame 1/1  ");
	JButton save = new GradientButton(Color.black,Color.red);
	JButton load = new GradientButton(Color.magenta,Color.cyan);
	Timer animator = new Timer(100,new Chicken());
	JButton original = new JButton("1st Position");
	JSlider speed = new JSlider(JSlider.HORIZONTAL,20,500,100);
	JLabel spdLbl = new JLabel("Speed dial (Slower -->)");
	JLabel wdLbl = new JLabel("Width of lines: 1");
	JLabel dmLbl = new JLabel("Diameter of circles: 6");
	JLabel nLbl = new JLabel("  Animation Controls: ");
	JButton insert = new JButton("Insert this frame");
	JButton delete = new JButton("Delete");
	JCheckBox bkSpd = new JCheckBox("Back goes to beginning of animation");
	JCheckBox fdSpd = new JCheckBox("Next goes to end of animation");
	JCheckBox loop = new JCheckBox("Loop when done with the animation");
	JCheckBox  onTop = new JCheckBox("Stick Figure is on top");
	JButton quit = new JButton("Quit");
	JButton addBall = new JButton("Add a ball");
	JButton delBall = new JButton("Rid of balls");
	boolean paused = false;
	boolean bkSpeed = false;
	boolean fdSpeed = false;
	static boolean top = false;
	int jjj = 1;
	boolean q = false;
	JSlider width = new JSlider(JSlider.HORIZONTAL,1,25,1);
	JSlider diameter = new JSlider(JSlider.HORIZONTAL,4,50,6);

	//***********
	int count = 1;
	int current = 1;
	int timer = 0;
	ArrayList<StickFigure> frames = new ArrayList<StickFigure>();
	ArrayList<BallList> list = new ArrayList<BallList>();
	//***********

	StickAnimator()
	{
		delBall.addActionListener(this);
		onTop.addActionListener(this);
		onTop.addMouseListener(new Pork());
		onTop.setBackground(null);
		onTop.setBounds(460,200,230,20);
		pain.add(onTop);
		delBall.addMouseListener(new Pork());
		addBall.addActionListener(this);
		addBall.addMouseListener(new Pork());
		addBall.setBackground(Color.white);
		pain.add(addBall);
		pain.setBackground(Color.white);
		setResizable(false);
		pause.setEnabled(false);
		stop.setEnabled(false);
		newFrame.setBackground(new Color(140,0,0));
		newFrame.setForeground(new Color(250,190,0));
		original.addMouseListener(new Pork());
		original.addActionListener(this);
		setTitle("Stick Figure Animation Program");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		centerFrame(710,550);
		loop.setBackground(null);
		bkSpd.setBounds(460,220,230,20);
		pain.add(bkSpd);
		fdSpd.setBounds(460,240,200,20);
		bkSpd.addActionListener(this);
		fdSpd.addActionListener(this);
		bkSpd.addMouseListener(new Pork());
		fdSpd.addMouseListener(new Pork());
		bkSpd.setBackground(null);
		fdSpd.setBackground(null);
		pain.add(fdSpd);
		buttons.setBounds(10,410,690,100);
		load.setBounds(470,40,210,25);
		addBall.setBounds(470,100,100,25);
		delBall.setBounds(570,100,110,25);
		pain.add(delBall);
		delBall.setBackground(Color.white);
		pain.add(load);
		load.setText("Load a stick Animation");
		save.setText("Save this Animation");
		save.setForeground(Color.white);
		speed.addMouseListener(new Pork());
		save.setBounds(470,70,210,25);
		speed.setBounds(470,170,210,25);
		pain.add(speed);
		speed.addChangeListener(this);
		pain.add(save);
		quit.addActionListener(this);
		quit.addMouseListener(new Pork());
		spdLbl.setBounds(470,150,165,25);
		pain.add(spdLbl);
		buttons.setBorder(new TitledBorder(new LineBorder(new Color(20,100,20)),"Buttons"));
		buttons.add(newFrame);
		buttons.add(clr);
		buttons.add(original);
		buttons.add(delete);
		buttons.add(ind);
		buttons.add(insert);
		buttons.add(back);
		buttons.add(foreward);
		buttons.add(nLbl);
		speed.setBackground(null);
		buttons.add(animate);
		buttons.add(pause);
		loop.setBounds(460,260,230,20);
		pain.add(loop);
		loop.addActionListener(this);
		loop.addMouseListener(new Pork());
		buttons.add(stop);
		buttons.add(quit);
		pain.add(buttons);
		drawFigure.addMouseListener(new Pork());
		drawFigure.setBorder(new TitledBorder(new CompoundBorder(
			new LineBorder(new Color(93,93,93)),
			new SoftBevelBorder(SoftBevelBorder.RAISED)),
			"Drag the dots to create & press 'Add Frame' to make a new frame"));

		buttons.setBackground(new Color(185,250,185));
		clr.setBackground(Color.red);
		save.addMouseListener(new Pork());
		save.addActionListener(this);
		load.addMouseListener(new Pork());
		load.addActionListener(this);
		newFrame.addMouseListener(new Pork());
		newFrame.addActionListener(this);
		original.setBackground(Color.orange);
		clr.addMouseListener(new Pork());
		clr.addActionListener(this);
		insert.addActionListener(this);
		insert.addMouseListener(new Pork());
		back.setBackground(Color.blue);
		back.setForeground(new Color(150,200,150));
		delete.addActionListener(this);
		delete.addMouseListener(new Pork());
		foreward.setBackground(Color.black);
		foreward.setForeground(Color.white);
		delete.setBackground(Color.yellow);
		back.addMouseListener(new Pork());
		back.addActionListener(this);
		insert.setBackground(Color.green);
		foreward.addMouseListener(new Pork());
		foreward.addActionListener(this);
		animate.setBackground(Color.cyan);
		animate.addMouseListener(new Pork());
		animate.addActionListener(this);
		pause.setBackground(Color.magenta);
		pause.addMouseListener(new Pork());
		pause.addActionListener(this);
		stop.addMouseListener(new Pork());
		stop.addActionListener(this);
		addMouseListener(new Pork());
		drawFigure.setBounds(10,10,450,390);
		pain.add(drawFigure);
		pause.setBackground(Color.LIGHT_GRAY);
		stop.setBackground(Color.LIGHT_GRAY);
		diameter.setBounds(470,320,210,25);
		width.setBounds(470,370,210,25);
		pain.add(diameter);
		pain.add(width);
		width.addMouseListener(new Pork());
		width.addChangeListener(this);
		width.setBackground(null);
		diameter.addMouseListener(new Pork());
		diameter.addChangeListener(this);
		diameter.setBackground(null);
		diameter.setMajorTickSpacing(2);
		dmLbl.setBounds(490,300,190,20);
		pain.add(dmLbl);
		wdLbl.setBounds(490,350,190,20);
		pain.add(wdLbl);
		diameter.setSnapToTicks(true);
		//*******
		setVisible(true);
	}
	public void centerFrame(int frameWidth, int frameHeight)
	{
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width-frameWidth)/2;
		int yUpperLeftCorner = (screen.height-frameHeight)/2;

		setBounds(xUpperLeftCorner,yUpperLeftCorner,frameWidth,frameHeight);
	}
	public void actionPerformed(ActionEvent evt)
	{
		try
		{
			if(evt.getSource() == animate)
			{
				jjj = 1;
				paused = false;
				pain.setBackground(Color.orange);
				animate.setText("Animate");
				animator.start();
				pause.setEnabled(true);
				stop.setEnabled(true);
				load.setForeground(Color.LIGHT_GRAY);
				save.setForeground(Color.LIGHT_GRAY);
				enableButtons(false);
				stop.setBackground(Color.pink);
				pause.setBackground(Color.magenta);
				drawFigure.animate();
			}
			else if(evt.getSource() == newFrame)
			{
				StickFigure sf = new StickFigure(drawFigure.getPoints());
				sf.setOnTop(top);
				frames.add(sf);
				list.add(new BallList(drawFigure.getBalls()));
				count++;
				current++;
				ind.setText("  On frame "+current+"/"+count+"  ");
			}
			else if(evt.getSource() == pause)
			{
				animator.stop();
				pause.setEnabled(false);
				stop.setEnabled(false);
				animate.setText("Resume");
				pause.setBackground(Color.LIGHT_GRAY);
				stop.setBackground(Color.LIGHT_GRAY);
				paused = true;
			}
			else if(evt.getSource() == stop)
			{
				paused = false;
				animator.stop();
				pause.setEnabled(false);
				stop.setEnabled(false);
				animate.setText("Animate");
				enableButtons(true);
				if(count > 1&& current < count)
					drawFigure.drawFigure(frames.get(current-1),list.get(current-1));
				else if(current == count)
				{
					drawFigure.drawFigure(frames.get(current-2),list.get(current-2));
				}
				pause.setBackground(Color.LIGHT_GRAY);
				stop.setBackground(Color.LIGHT_GRAY);
				pain.setBackground(Color.white);
				timer = 0;
				drawFigure.deAnimate();
			}
			else if(evt.getSource() == foreward)
			{
				if(current < count)
				{
					if(fdSpeed)
					{
						current = (frames.size()+1);
						drawFigure.drawFigure(frames.get(current-2),list.get(current-2));
						ind.setText("  On frame "+current+"/"+count+"  ");
					}
					else
					{
						System.out.println(current);
						drawFigure.drawFigure(frames.get(current-1),list.get(current-1));
						current++;
						ind.setText("  On frame "+current+"/"+count+"  ");
					}
				}
			}
			else if(evt.getSource() == back)
			{
				if(current > 1)
				{
					if(bkSpeed)
					{
						current = 1;
						drawFigure.drawFigure(frames.get(current-1),list.get(current-1));
						ind.setText("  On frame "+current+"/"+count+"  ");
					}
					else
					{
						System.out.println(current);
						drawFigure.drawFigure(frames.get(current-2),list.get(current-2));
						current--;
						ind.setText("  On frame "+current+"/"+count+"  ");
					}
				}
			}
			else if(evt.getSource() == clr)
			{
				setAlwaysOnTop(false);
				int ans43 = JOptionPane.showConfirmDialog(this,"Do you realy want to delete all the slides??");
				if(ans43 == 0)
				{
					frames.clear();
					list.clear();
					drawFigure.clrBalls();
					drawFigure.drawFigure(drawFigure.predefinedPoints);
					current = 1;
					count = 1;
					ind.setText("  On frame "+current+"/"+count+"  ");
					timer = 0;
				}
			}
			else if(evt.getSource() == load)
			{
				setAlwaysOnTop(false);
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new File("C:/WINDOWS/Desktop/olivers documents/7th grade/Computer Programming"+
					"/JFrame templates, etc/Stick FIgures/"));
				chooser.setFileFilter(new javax.swing.filechooser.FileFilter()
					{
						public boolean accept(File f)
						{
							return (f.getName().endsWith("q.ser")||f.getName().endsWith("q.txt")||
								f.isDirectory());
						}
						public String getDescription()
						{
							return "Animation (q.ser or q.txt)";
						}
					}
				);
				int r = chooser.showOpenDialog(this);
				if(r == JFileChooser.APPROVE_OPTION)
				{
					try
					{
						File file = new File(chooser.getSelectedFile().getAbsolutePath());
						if(file.getName().endsWith("q.txt"))
						{
							JOptionPane.showMessageDialog(null,"Please try to resave this as a \".ser\" document.");
							BufferedReader rd = new BufferedReader(new FileReader(file.getAbsolutePath()));
							frames.clear();
							list.clear();
							count = 1;
							current = 1;
							timer = 0;

							while(true)
							{
								String tempStr = rd.readLine();
								if(tempStr == null)
									break;
								frames.add(StickFigurePanel.toFigure(tempStr));
								list.add(StickFigurePanel.toBallList(tempStr));
								count++;
								current++;
							}
							drawFigure.drawFigure(frames.get(frames.size()-1),list.get(frames.size()-1));
							ind.setText("  On frame "+current+"/"+count+"  ");
							rd.close();
							setTitle("Stick Figure Animation Program:  "+chooser.getSelectedFile().getName());
						}
						else if(file.getName().endsWith("q.ser"))
						{
							ObjectInputStream ins = new ObjectInputStream(new FileInputStream(file.getAbsolutePath()));
							StickFigure tempStick;
							BallList tempBall;
							frames.clear();
							list.clear();
							count = 1;
							current = 1;
							timer = 0;

							while(true)
							{
								try
								{
									tempStick = (StickFigure)ins.readObject();
									tempBall = (BallList)ins.readObject();

									frames.add(tempStick);
									list.add(tempBall);
									count++;
									current++;
								}
								catch(EOFException e)
								{
									break;
								}
							}
							drawFigure.drawFigure(frames.get(frames.size()-1),list.get(frames.size()-1));
							ind.setText("  On frame "+current+"/"+count+"  ");
							ins.close();
							setTitle("Stick Figure Animation Program:  "+chooser.getSelectedFile().getName());
						}
					}
					catch(Exception e)
					{
						String exception = e+"";

						if(exception.equals("java.lang.ArrayIndexOutOfBoundsException: -1"))
						{
							int ans2 = JOptionPane.showConfirmDialog(this,"Blank file: "+
								chooser.getSelectedFile().getName()+"\n\nDo you wish to delete the file?");
							if(ans2 == 0)
								new File(chooser.getSelectedFile().getAbsolutePath()).delete();
						}
						else
							JOptionPane.showMessageDialog(this,"Exception: "+e);
					}
				}
			}
			else if(evt.getSource() == save)
			{
				setAlwaysOnTop(false);
				JFileChooser chooser = new JFileChooser();
				chooser.setFileFilter(new javax.swing.filechooser.FileFilter()
					{
						public boolean accept(File f)
						{
							return (f.getName().endsWith("q.ser")||f.getName().endsWith("q.SER")||
								f.isDirectory());
						}
						public String getDescription()
						{
							return "Animation";
						}
					}
				);
				chooser.setCurrentDirectory(new File("C:/WINDOWS/Desktop/olivers documents/7th grade/Computer Programming"+
					"/JFrame templates, etc/Stick FIgures/"));
				int r = chooser.showSaveDialog(this);
				if(r == JFileChooser.APPROVE_OPTION)
				{
					String dir = chooser.getSelectedFile().getAbsolutePath();
					if(!(dir.endsWith("q.ser")||dir.endsWith("q.SER")))
						dir+="q.ser";
					System.out.println("The file name is: "+dir);
					File file = new File(dir);
					if(!file.exists())
					{
						try
						{
							file.createNewFile();
							ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file.getName()));

							for(int i = 0; i < frames.size(); i++)
							{
								out.writeObject(frames.get(i));
								out.writeObject(list.get(i));
							}
							out.close();
							setTitle("Stick Figure Animation Program:  "+chooser.getSelectedFile().getName());
						}
						catch(Exception e){}
					}
					else
					{
						int ans = JOptionPane.showConfirmDialog(this,
							"Do you want to replace the last file?");

						switch(ans)
						{
							case 1: 	try
										{
											ObjectOutputStream out = new ObjectOutputStream(
													new FileOutputStream(file.getName(),false));

											for(int i = 0; i < frames.size(); i++)
											{
												out.writeObject(frames.get(i));
												out.writeObject(list.get(i));
											}
											out.close();
											setTitle("Stick Figure Animation Program:  "+chooser.getSelectedFile().getName());
										}
										catch(Exception e){}
										break;
							case 2:		int ans2 = JOptionPane.showConfirmDialog(this,
											"Do you want to add to the last file?");

										if(ans == 0)
										{
											try
											{
												ObjectOutputStream out = new ObjectOutputStream(
														new FileOutputStream(file.getName(),false));

												for(int i = 0; i < frames.size(); i++)
												{
													out.writeObject(frames.get(i));
													out.writeObject(list.get(i));
												}
												out.close();
												setTitle("Stick Figure Animation Program:  "+chooser.getSelectedFile().getName());
											}
											catch(Exception e){}
										}
										break;
						}
					}
				}
			}
			else if(evt.getSource() == original)
			{
				drawFigure.drawFigure(drawFigure.predefinedPoints);
			}
			else if(evt.getSource() == insert)
			{
				try
				{
					setAlwaysOnTop(false);
					int pos = Integer.parseInt(JOptionPane.showInputDialog(this,"Insert this slide after wh"+
						"ich slide?\nEnter the slide number of the slide you wish to have before that slide","Insert "+
						"title here",3));
					StickFigure sf = new StickFigure(drawFigure.getPoints());
					sf.setOnTop(top);
					frames.add((pos+1),sf);
					count++;
					current = (pos+1);
					ind.setText("  On frame "+current+"/"+count+"  ");
				}
				catch(NumberFormatException nfe)
				{
					JOptionPane.showMessageDialog(this,"No slide was inserted due to improper input");
				}
			}
			else if(evt.getSource() == delete)
			{
				if(current <(count-1))
				{
					frames.remove((current-1));
					list.remove(current-1);
					drawFigure.drawFigure(frames.get((current-1)),list.get(current-1));
					count--;
					ind.setText("  On frame "+current+"/"+count+"  ");
				}
			}
			else if(evt.getSource() == fdSpd)
			{
				if(fdSpd.isSelected())
					fdSpeed = true;
				else
					fdSpeed = false;
			}
			else if(evt.getSource() == bkSpd)
			{
				if(bkSpd.isSelected())
					bkSpeed = true;
				else
					bkSpeed = false;
			}
			else if(evt.getSource() == loop)
			{
				if(loop.isSelected())
					q = true;
				else
					q = false;
			}
			else if(evt.getSource() == quit)
				System.exit(0);
			else if(evt.getSource() == addBall)
			{
				drawFigure.addBall(new Ball(50,new Point(400,360),new Color((int)(Math.random()*255),
					(int)(Math.random()*255),(int)(Math.random()*255))));
			}
			else if(evt.getSource() == delBall)
			{
				drawFigure.clrBalls();
				drawFigure.drawFigure(new StickFigure(drawFigure.getPoints()));
			}
			else if(evt.getSource() == onTop)
			{
				if(onTop.isSelected())
					top = true;
				else
					top = false;

			}

		}
		catch(Exception e)
		{
			setAlwaysOnTop(false);
			e.printStackTrace();
			JOptionPane.showMessageDialog(this,"The Error in the ActionPerformedMethod()\n\n"+e,"ERROR",0);
		}
	}
	private class Chicken implements ActionListener
	{
		public void actionPerformed(ActionEvent evt)
		{
			if(timer < (count-1))
			{
				drawFigure.drawFigure(frames.get(timer),list.get(timer));
				timer++;
			}
			else
			{
				timer = 0;
				if(!q)
				{
					animator.stop();
					pause.setEnabled(false);
					stop.setEnabled(false);
					animate.setText("Animate");
					enableButtons(true);
					pause.setBackground(Color.LIGHT_GRAY);
					stop.setBackground(Color.LIGHT_GRAY);
					pain.setBackground(Color.white);
					drawFigure.deAnimate();
					paused = false;
					if(count > 1&& current < count)
						drawFigure.drawFigure(frames.get(current-1),list.get(current-1));
				}
				else
				{
					System.out.println("REPEAT # "+jjj);
					jjj++;
				}
			}
		}
	}
	public void stateChanged(ChangeEvent evt)
	{
		if(evt.getSource() == speed)
			animator.setDelay(speed.getValue());
		else if(evt.getSource() == diameter)
		{
			drawFigure.setDiameter(diameter.getValue());
			dmLbl.setText("Diameter of circles: "+diameter.getValue());
		}
		else if(evt.getSource() == width)
		{
			drawFigure.setWidth(width.getValue());
			wdLbl.setText("Width of lines: "+width.getValue());
		}
	}
	private class Pork extends MouseAdapter implements MouseListener
	{
		public void mouseEntered(MouseEvent evt)
		{
			setAlwaysOnTop(false);
			if(!animator.isRunning()&&!paused)
				pain.setBackground(Color.white);
		}
		public void mouseExited(MouseEvent evt)
		{
			if(!animator.isRunning()&&!paused)
				pain.setBackground(new Color(255,255,130));
		}
	}

	public void enableButtons(boolean x)
	{
		newFrame.setEnabled(x);
		clr.setEnabled(x);
		load.setEnabled(x);
		save.setEnabled(x);
		addBall.setEnabled(x);
		delBall.setEnabled(x);
		onTop.setEnabled(x);
		if(x)
		{
			save.setForeground(Color.white);
			load.setForeground(Color.black);
		}
		else
		{
			save.setForeground(Color.LIGHT_GRAY);
			load.setForeground(Color.LIGHT_GRAY);
		}
		fdSpd.setEnabled(x);
		bkSpd.setEnabled(x);
		original.setEnabled(x);
		insert.setEnabled(x);
		delete.setEnabled(x);
		back.setEnabled(x);
		foreward.setEnabled(x);
		loop.setEnabled(x);
	}
	public static void main(String args[])
	{
		new StickAnimator();
	}
}