import java.awt.*;
import javax.swing.*;

class GradientButton extends JButton
{
	Color one = null;
	Color two = null;
	Color fore = Color.black;
	String t = "";

	GradientButton(Color c1, Color c2)
	{
		one = c1;
		two = c2;
		repaint();
	}
	public void paintComponent(Graphics g)
	{
		Graphics2D g2d = (Graphics2D)g;
		super.paintComponent(g2d);
		g2d.setPaint(new GradientPaint(0,0,one,getWidth(),getHeight(),two));
		g2d.fillRect(0,0,getWidth(),getHeight());
		g2d.setPaint(fore);
		g2d.setFont(new Font("SERIF",Font.BOLD,14));
		g2d.drawString(t,20,17);
	}
	public void setText(String s)
	{
		t = s;
		repaint();
	}
	public void setForeground(Color c3)
	{
		fore = c3;
	}
}