import java.awt.*;
import java.io.*;
import javax.imageio.*;
import java.util.*;
import java.awt.geom.*;
import java.awt.image.*;

public abstract class Platform extends GameObject
{
	public static BufferedImage stone;

	static
	{
		try
		{
			stone = ImageIO.read(new File("STONE_BRICK.jpg"));
		}catch(Exception e){}
	}

	public abstract void support(PhysicsObject p);

	//********************************************************************************
	//********************************************************************************

	public static class BoxPlatform extends Platform
	{
		BufferedImage img = null;
		double e = .2;

		public BoxPlatform(int x, int y, int w, int h)
		{
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}

		public Image paint()
		{
			if(img == null || w != img.getWidth() || h!= img.getHeight())
			{
				img = new BufferedImage(w,h, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = img.createGraphics();

				g.setPaint(new TexturePaint(stone,
					new Rectangle2D.Double(0,0,stone.getWidth(),stone.getHeight())));
				g.fillRect(0,0,w,h);

				g.dispose();
			}
			return img;
		}

		public void update()
		{
		}

		public boolean intersects(Polygon p)
		{
			return p.intersects(new Rectangle(x,y,w,h));
		}

		public Polygon getPolygon()
		{
			return new Polygon(new int[]{x,x+w,x+w,x}, new int[]{y,y,y+h,y+h}, 4);
		}

		public void support(PhysicsObject p)// physics section
		{
			/*if(p.y + p.h -p.v.y < y)//top
			{
				//System.out.println("("+x+","+y+","+w+","+h+"\t"+p.x+","+p.y+","+p.w+","+p.h);
				p.v.y = Math.min(-p.v.y*e,p.v.y);
				p.y = y-p.h;
				p.landed = 0;
			}
			else if(p.y-p.v.y > y + h && (p.x < x+w && x < p.x+p.w))//bottom1
			{
				p.v.y = Math.max(-p.v.y*e,p.v.y);
				p.y = y+h;
			}
			else if(p.x - p.v.x > x + w && (p.y < y+h && y < p.y+p.h))//right
			{
				p.v.x = Math.max(-p.v.x*e, p.v.x);
				p.x = x+w;
			}
			else if(p.x + p.w - p.v.x < x && (p.y < y+h && y < p.y+p.h))//left
			{
				p.v.x = Math.min(-p.v.x*e, p.v.x);
				p.x = x-p.w;
			}
			else
			{
			}*/



			ArrayList<Vector2D> forces = new ArrayList<Vector2D>();
			ArrayList<Vector2D> pos = new ArrayList<Vector2D>();

			//while(p.intersects(getPolygon()))
		//	{
		//		p.x -= p.v.x/2;
		//		p.y -= p.v.y/2;
		//	}

		//	p.x += p.v.x;
		//	p.y += p.v.y;

			if(p.intersection(getPolygon(), forces, pos))
			{
				for(int i = 0; i < forces.size(); i++)
					if(Math.abs(forces.get(i).magnitude()) <= .01)
						forces.remove(forces.get(i--));

				for(int i = 0; i < forces.size(); i++)
					System.out.println(forces.get(i));

				System.out.println();
				p.landed = 0;

				for(Vector2D v : forces)
				{
					v.unitize();
					v.scale(p.v.magnitude()/2);
				}
				p.handleImpulses(forces, pos);
			}
		}
	}

	//********************************************************************************
	//********************************************************************************

	public static class LinePlatform extends Platform
	{
		BufferedImage img = null;

		private int x1, y1, x2, y2, width = 20;

		public LinePlatform(int x, int y, int x2, int y2)
		{
			this.x = Math.min(x,x2);
			this.y = Math.min(y,y2);
			this.w = Math.abs(x2-x);
			this.h = Math.abs(y2-y);

			this.x1 = x;
			this.x2 = x2;
			this.y1 = y;
			this.y2 = y2;
		}

		public Polygon getPolygon()
		{
			int dx = (int)(width*(y2-y1)/Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1))),
				dy = (int)(width*(x2-x1)/Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1)));
			return new Polygon(new int[]{x1-dx, x1+dx, x2+dx, x2-dx},
				new int[]{y1+dy, y1-dy, y2-dy, y2+dy}, 4);
		}

		public Image paint()
		{
			if(img == null || w != img.getWidth() || h!= img.getHeight())
			{
				img = new BufferedImage(w,h, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = img.createGraphics();

				g.setPaint(new TexturePaint(stone,
					new Rectangle2D.Double(0,0,stone.getWidth(),stone.getHeight())));
				//g.setColor(Color.DARK_GRAY);
				g.setStroke(new BasicStroke(width));

				g.drawLine(x1-x, y1-y, x2-x, y2-y);

				g.dispose();
			}
			return img;
		}

		public void update()
		{
		}

		public void support(PhysicsObject p)
		{
			ArrayList<Vector2D> forces = new ArrayList<Vector2D>();
			ArrayList<Vector2D> pos = new ArrayList<Vector2D>();

			if(p.intersection(getPolygon(), forces, pos))
			{
				for(Vector2D v : forces)
				{
				}

				p.handleImpulses(forces,pos);
			}
		}
	}

	//********************************************************************************
	//*******************************************************************************

	public static class Fence extends Platform
	{

		BufferedImage img = null;

		public Fence(int x, int y, int w, int h)
		{
			this.x = x;
			this.y = y;
			this.w = w;
			this.h = h;
		}

		public Polygon getPolygon()
		{
			return new Polygon(new int[]{x,x+w,x+w,x}, new int[]{y,y,y+h,y+h}, 4);
		}

		public Image paint()
		{
			if(img == null || w != img.getWidth() || h!= img.getHeight())
			{
				img = new BufferedImage(w,h, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = img.createGraphics();

				g.setColor(new Color(175,103,33));
				g.setStroke(new BasicStroke(20));
				for(int x = 0; x < w; x+=80)
					for(int y = 0; y < h; y+=80)
					{
						g.drawLine(x+80,y+80,x,y);
						g.drawLine(x+80,y,x,y+80);
					}

				g.drawRect(0,0,w,h);

				g.dispose();
			}
			return img;
		}

		public void update()
		{
		}

		public void support(PhysicsObject p)
		{
			p.landed = 1;
		}
	}

	public static void main(String[] args)
	{
		PlatformDriver.main(args);
	}
}