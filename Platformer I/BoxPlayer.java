import java.awt.*;
import java.awt.image.*;
import java.awt.event.KeyEvent;

abstract class BoxPlayer extends Player
{
	public static class Alligned extends BoxPlayer
	{
		BufferedImage image = null;

		public Alligned()
		{
			w = 60;
			h = 60;
		}

		public void update()
		{
			y += Math.signum(v.y)*Math.floor(Math.abs(v.y));
			x += Math.signum(v.x)*Math.floor(Math.abs(v.x));

			if(landed == -1)
				v.y += .2;

			v.x *= .96;
			if(landed==1)
				v.y *= .96;
		}

		public void setAngle(double t)
		{
		}

		public double getAngle()
		{
			return 0;
		}

		public boolean intersects(Polygon p)
		{
			return p.intersects(new Rectangle(x,y,w,h));
		}

		public Polygon getPolygon()
		{
			return new Polygon(new int[]{x,x+w,x+w,x},new int[]{y,y,y+h,y+h},4);
		}

		public void react(java.util.Set<Integer> down)
		{
			if(down.contains(KeyEvent.VK_A))
				v.x += (-5-v.x)/4d;
			if(down.contains(KeyEvent.VK_D))
				v.x += (5-v.x)/4d;
			if(down.contains(KeyEvent.VK_W))
			{
				if(landed == 0)
					v.y = -10;
				else if(landed == 1)
					v.y += (-5-v.y)/4d;
			}
			if(down.contains(KeyEvent.VK_S))
			{
				if(landed == 0);
				else if(landed == 1)
					v.y += (5-v.y)/4d;
			}
			if(down.contains(KeyEvent.VK_SPACE))
			{
				v.y -= 5;
			}
		}

		public Image paint()
		{
			//if(image == null || w != image.getWidth() || h!= image.getHeight())
			{
				image = new BufferedImage(w,h, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = image.createGraphics();

				g.setColor(Color.RED);
				g.fillRect(0,0,w,h);

				g.dispose();

			}

			return image;
		}

	}

	//*************************************************************************************

	public static class Rotatable extends BoxPlayer
	{
		protected double angle = 0;

		int rw, rh;
		BufferedImage image = null;

		public Rotatable()
		{
			w = 60;
			h = 80;
			rw = 60;
			rh = 80;
		}


		public Polygon getPolygon()
		{
			return new Polygon(
				new int[]{x + w/2 +(int)Methods.getRotateX(-rw/2,-rh/2,angle),
					x + w/2 + (int)Methods.getRotateX(rw/2,-rh/2,angle),
					x + w/2 + (int)Methods.getRotateX(rw/2,rh/2,angle),
					x + w/2 + (int)Methods.getRotateX(-rw/2,rh/2,angle)},

				new int[]{y + h/2 + (int)Methods.getRotateY(-rw/2,-rh/2,angle),
					y + h/2 + (int)Methods.getRotateY(rw/2,-rh/2,angle),
					y + h/2 + (int)Methods.getRotateY(rw/2,rh/2,angle),
					y + h/2 + (int)Methods.getRotateY(-rw/2,rh/2,angle)}, 4);
		}

		public void update()
		{
			//System.out.println("x: "+x+"\ty: "+y+"\tvx: "+v.x+"\tvy: "+v.y);
			y += Math.signum(v.y)*Math.floor(Math.abs(v.y));
			x += Math.signum(v.x)*Math.floor(Math.abs(v.x));

			if(landed == -1)
				v.y += .2;

			v.x *= .96;
			if(landed==1)
				v.y *= .96;

			setAngle(angle+.01);
		}

		public void setAngle(double t)
		{
			int nw = (int)(rw*Math.abs(Math.cos(t))+ rh*Math.abs(Math.sin(t))),
				nh = (int)(rw*Math.abs(Math.sin(t))+ rh*Math.abs(Math.cos(t)));

			x -= (nw - w);
			y -= (nh - h);

			w= nw;
			h = nh;

			angle = t;
		}

		public double getAngle()
		{
			return angle;
		}

		public void react(java.util.Set<Integer> down)
		{
			if(down.contains(KeyEvent.VK_A))
				v.x += (-5-v.x)/4d;
			if(down.contains(KeyEvent.VK_D))
				v.x += (5-v.x)/4d;
			if(down.contains(KeyEvent.VK_W))
			{
				if(landed == 0)
					v.y = -10;
				else if(landed == 1)
					v.y += (-5-v.y)/4d;
			}
			if(down.contains(KeyEvent.VK_S))
			{
				if(landed == 0);
				if(landed == 1)
					v.y += (5-v.y)/4d;
			}
		}

		public Image paint()
		{
			//if(image == null || w != image.getWidth() || h!= image.getHeight())
			{
				image = new BufferedImage(w,h, BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = image.createGraphics();
				g.translate(w/2, h/2);
				g.rotate(angle);

				g.setColor(Color.BLUE);
				g.fillRect(-rw/2,-rh/2,rw,rh);

				g.rotate(-angle);
				g.translate(-w/2, -h/2);
				g.dispose();
			}

			return image;
		}
	}

	public static void main(String[] args)
	{
		PlatformDriver.main(args);
	}
}