import javax.swing.*;
import java.awt.*;

class PlatformDriver extends JFrame
{
	public static final Dimension SCREEN = Toolkit.getDefaultToolkit().getScreenSize();

	PlatformDisplay display;

	public PlatformDriver()
	{
		PlatformWorld world = new PlatformWorld();

		//EXAMPLE CODE***********
		world.add(new Platform.BoxPlatform(100,800,500,100));
		world.add(new Platform.BoxPlatform(-400,1000,2000,40));
		world.add(new Platform.BoxPlatform(-400,800,40,200));
		world.add(new Platform.BoxPlatform(1400,800,200,200));
		world.add(new Goomba(200,500, 100,100));
		world.add(new Platform.Fence(400,0,200,600));
		world.add(new Platform.LinePlatform(600,800,1200,1000));
		//END EXAMPLE CODE*******

		Player player = new BoxPlayer.Rotatable();
		display =  new PlatformDisplay(world,player);


		setLayout(new BorderLayout());

		add(display, BorderLayout.CENTER);
		addKeyListener(display);

		setUndecorated(true);

		setCursor(Toolkit.getDefaultToolkit().createCustomCursor(
		    new java.awt.image.BufferedImage(16, 16, java.awt.image.BufferedImage.TYPE_INT_ARGB),
		    new Point(0, 0), "blank cursor"));

		setSize(SCREEN);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args)
	{
		PlatformDriver pd = new PlatformDriver();
		pd.setVisible(true);
	}
}