import java.awt.*;
import java.util.*;

public class Methods
{
	public static boolean intersects(int a, int b, int c, int d, int x, int y, int w, int h)
	{
		if(a+c < x || x + w < a || b+d < y || y + h < b) return false;
		return true;
	}

	public static boolean intersects(GameObject a, GameObject b)
	{
		return intersects(a.x,a.y,a.w,a.h, b.x,b.y,b.w,b.h);
	}

	public static boolean intersects(java.awt.Polygon a, java.awt.Polygon b)
	{
		return intersection(a,b,new ArrayList<Vector2D>(), new ArrayList<Vector2D>());
	}

	public static boolean intersection(java.awt.Polygon a, java.awt.Polygon b,
		java.util.List<Vector2D> force, java.util.List<Vector2D> pos)
	{
		pos.clear();
		force.clear();

		for(int i = 0; i < a.npoints; i++)
			if(b.contains(new Point(a.xpoints[i],a.ypoints[i])))
			{
				Vector2D[] pts = new Vector2D[b.npoints];

				for(int j = 0; j < b.npoints; j++)
					pts[j] = closest(
						new Vector2D(b.xpoints[j], b.ypoints[j]),
						new Vector2D(b.xpoints[(j+1)%b.npoints], b.ypoints[(j+1)%b.npoints]),
							new Vector2D(a.xpoints[i],a.ypoints[i]));

				Vector2D min = pts[0];
				for(Vector2D v : pts)
					if(v.magnitude() < min.magnitude())
						min = v;

				min.negate();

				force.add(min);
				pos.add(new Vector2D(a.xpoints[i],a.ypoints[i]));
			}

		for(int i = 0; i < b.npoints; i++)
			if(a.contains(new Point(b.xpoints[i],b.ypoints[i])))
			{
				Vector2D[] pts = new Vector2D[b.npoints];

				for(int j = 0; j < a.npoints; j++)
					pts[j] = closest(
						new Vector2D(a.xpoints[j], a.ypoints[j]),
						new Vector2D(a.xpoints[(j+1)%a.npoints], a.ypoints[(j+1)%a.npoints]),
							new Vector2D(b.xpoints[i],b.ypoints[i]));

				Vector2D min = pts[0];
				for(Vector2D v : pts)
					if(v.magnitude() < min.magnitude())
						min = v;

				force.add(min);
				pos.add(new Vector2D(b.xpoints[i],b.ypoints[i]));
			}

		if(force.isEmpty())
			return false;

		return true;

	}

	public static Vector2D closest(Vector2D a, Vector2D b, Vector2D q)
	{
		Vector2D ba = b.minus(a), qa = q.minus(a);
		return qa.minus(ba.mult(ba.dot(qa)/ba.dot(ba)));
	}

	public static Point m()
	{
		return MouseInfo.getPointerInfo().getLocation();
	}

	public static double getRotateX(double x, double y, double angle)
	{
		return x*Math.cos(angle)-y*Math.sin(angle);
	}
	public static double getRotateY(double x, double y, double angle)
	{
		return y*Math.cos(angle)+x*Math.sin(angle);
	}

	// calculates intersection and checks for parallel lines.
	// also checks that the intersection point is actually on
	// the line segment p1-p2
	public static Point findIntersection(Point p1,Point p2, Point p3,Point p4)
	{
		double xD1,yD1,xD2,yD2,xD3,yD3;
		double dot,deg,len1,len2;
		double segmentLen1,segmentLen2;
		double ua,ub,div;

		// calculate differences
		xD1 = p2.x-p1.x;
		xD2 = p4.x-p3.x;
		yD1 = p2.y-p1.y;
		yD2 = p4.y-p3.y;
		xD3 = p1.x-p3.x;
		yD3 = p1.y-p3.y;

		// calculate the lengths of the two lines
		len1 = Math.sqrt(xD1*xD1+yD1*yD1);
		len2 = Math.sqrt(xD2*xD2+yD2*yD2);

		// calculate angle between the two lines.
		dot = (xD1*xD2+yD1*yD2); // dot product
		deg = dot/(len1*len2);

		// if abs(angle)==1 then the lines are parallell,
		// so no intersection is possible
		if(Math.abs(deg) == 1)
			return null;

		// find intersection Pt between two lines
		Point pt = new Point(0,0);
		div = yD2*xD1-xD2*yD1;
		ua = (xD2*yD3-yD2*xD3)/div;
		ub = (xD1*yD3-yD1*xD3)/div;
		pt.x = (int)(p1.x+ua*xD1);
		pt.y = (int)(p1.y+ua*yD1);

		// calculate the combined length of the two segments
		// between Pt-p1 and Pt-p2
		xD1 = pt.x-p1.x;
		xD2 = pt.x-p2.x;
		yD1 = pt.y-p1.y;
		yD2 = pt.y-p2.y;
		segmentLen1 = Math.sqrt(xD1*xD1+yD1*yD1)+Math.sqrt(xD2*xD2+yD2*yD2);

		// calculate the combined length of the two segments
		// between Pt-p3 and Pt-p4
		xD1=pt.x-p3.x;
		xD2=pt.x-p4.x;
		yD1=pt.y-p3.y;
		yD2=pt.y-p4.y;
		segmentLen2 = Math.sqrt(xD1*xD1+yD1*yD1)+Math.sqrt(xD2*xD2+yD2*yD2);

		// if the lengths of both sets of segments are the same as
		// the lenghts of the two lines the point is actually
		// on the line segment.

		// if the point isn't on the line, return null
		if(Math.abs(len1-segmentLen1) > 0.01 || Math.abs(len2-segmentLen2) > 0.01)
			return null;
		// return the valid intersection
		return pt;
	}
}