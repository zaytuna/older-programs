import java.awt.*;
import javax.imageio.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.io.File;

class Goomba extends Enemy
{
	private static BufferedImage left = null;
	private static BufferedImage right = null;

	int lastX = 0;

	public boolean dead = false;

	static
	{
		try
		{
			left = ImageIO.read(new File("GL.png"));
			right = ImageIO.read(new File("GR.png"));
		}catch(Exception e){}


	}

	public Goomba(int x, int y, int w, int h)
	{
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		v.x = 2;
	}

	public Image paint()
	{
		return v.x < 0? left:right;
	}

	public void interact(Player p)
	{
		if(p.y + p.h -p.v.y < y)
			dead = true;
		else
		{
			p.v.x = -Math.signum(p.v.x)*50;
			v.x = Math.signum(p.v.x)*10;
		}
	}

	public Polygon getPolygon()
	{
		return new Polygon(new int[]{x,x+w,x+w,x}, new int[]{y,y,y+h,y+h}, 4);
	}

	public void update()
	{
		if(dead)
			y += 10;
		else
		{
			v.x = 2*Math.signum(v.x);
			y += Math.signum(v.y)*Math.floor(Math.abs(v.y));
			x += Math.signum(v.x)*Math.floor(Math.abs(v.x));

			v.y += .2;

			lastX = x;
		}
	}

	public static void main(String[] args)
	{
		PlatformDriver pd = new PlatformDriver();
		pd.setVisible(true);
	}
}