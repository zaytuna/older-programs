import java.awt.*;
import java.awt.image.*;
import java.awt.event.KeyEvent;

public abstract class Player extends PhysicsObject
{
	boolean dead = false;

	public abstract void react(java.util.Set<Integer> keys);

	public static void main(String[] args)
	{
		PlatformDriver.main(args);
	}
}