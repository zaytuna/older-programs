import java.awt.Image;
import java.awt.Polygon;
import java.util.*;

public abstract class GameObject
{
	public int x, y, w, h;

	public abstract Image paint();
	public abstract void update();

	public boolean intersects(Polygon p)
	{
		return Methods.intersection(getPolygon(),p, new ArrayList<Vector2D>(), new ArrayList<Vector2D>());
	}

	public boolean intersection(Polygon p, List<Vector2D> f, List<Vector2D> o)
	{
		return Methods.intersection(getPolygon(),p,f,o);
	}
	public abstract Polygon getPolygon();
}