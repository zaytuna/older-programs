abstract class PhysicsObject extends GameObject
{
	public Vector2D v = new Vector2D(0,0);
	public int landed = -1;

	public void handleImpulse(Vector2D f, Vector2D pos)
	{
		handleImpulse(f);
	}

	public void handleImpulses(java.util.List<Vector2D> f, java.util.List<Vector2D> p)
	{
		for(Vector2D v : f)
			handleImpulse(v);
	}

	public Vector2D getCM()
	{
		return new Vector2D(w/2,h/2);
	}

	public void handleImpulse(Vector2D f)
	{
		v.x += f.x;
		v.y += f.y;
	}
}