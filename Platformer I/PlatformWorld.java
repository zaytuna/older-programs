import java.util.*;

public class PlatformWorld
{
	public List<Platform> platforms = new ArrayList<Platform>();
	public List<Enemy> enemies = new ArrayList<Enemy>();

	public void add(Platform p)
	{
		platforms.add(p);
	}

	public void add(Enemy e)
	{
		enemies.add(e);
	}

	public void remove(Enemy e)
	{
		enemies.remove(e);
	}

	public static void main(String[] args)
	{
		PlatformDriver.main(args);
	}
}