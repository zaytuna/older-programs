public abstract class Enemy extends PhysicsObject
{
	public abstract void interact(Player p);
}