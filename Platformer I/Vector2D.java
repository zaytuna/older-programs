public class Vector2D
{
	public double x, y;

	public Vector2D(double x, double y)
	{
		this.x = x;
		this.y = y;
	}

	public double get(int index)
	{
		return index == 0 ? x : y;
	}

	public double dot(Vector2D other)
	{
		return x * other.x + y * other.y;
	}

	public Vector2D minus(Vector2D other)
	{
		return new Vector2D(x-other.x, y-other.y);
	}


	public Vector2D plus(Vector2D other)
	{
		return new Vector2D(x+other.x,y+other.y);
	}

	public double magnitude()
	{
		return Math.sqrt(x * x + y * y);
	}

	public double angleBetween(Vector2D other)
	{
		return Math.acos(dot(other) / (magnitude() * other.magnitude()));
	}

	public void addToSelf(Vector2D v)
	{
		x += v.x;
		y += v.y;
	}

	public Vector2D mult(double s)
	{
		return new Vector2D(x*s, y*s);
	}


	public void scale(double s)
	{
		x *= s;
		y *= s;
	}

	public void negate()
	{
		x *= -1;
		y *= -1;
	}

	public void unitize()
	{
		x /= magnitude();
		y /= magnitude();
	}

	public String toString()
	{
		return "Vector2D (" + x + "," + y + ")";
	}
}
