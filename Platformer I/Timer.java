class Timer
{
	private static java.util.HashMap<String, Long> last = new java.util.HashMap<String, Long>();
	private static java.util.HashMap<String, Integer> called = new java.util.HashMap<String, Integer>();
	private static java.util.HashMap<String, Double> frozen = new java.util.HashMap<String, Double>();

	public static long nanoChange(String key)
	{
		long current = System.nanoTime();
		if(!last.containsKey(key))
			last.put(key,current);
		long dt =  current-last.get(key);

		last.put(key, current);

		return dt;
	}

	public static double getFPS(String key)
	{
		long current = System.nanoTime();

		if(!last.containsKey(key))
			last.put(key,current);
		long dt =  current-last.get(key);

		if(!called.containsKey(key))
			called.put(key, 0);

		if(dt > 1000000000L)
		{
			int c = called.get(key);
			called.put(key, 0);
			frozen.put(key, (double)(c*1000000000D/dt));
			last.put(key, current);
			return (double)(c*1000000000D/dt);
		}
		else
			called.put(key, called.get(key)+1);

		if(!frozen.containsKey(key))
			frozen.put(key, -1D);

		return frozen.get(key);
	}
}