import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.awt.event.*;

class PlatformDisplay extends JPanel implements Runnable, KeyListener
{
	public double GRAVITY = .5;

	int viewX, viewY, viewXTo, viewYTo;

	PlatformWorld world;
	Player player;

	Set<Integer> down = new HashSet<Integer>();

	public PlatformDisplay(PlatformWorld w, Player p)
	{
		this.world = w;
		this.player = p;

		addKeyListener(this);

		new Thread(this).start();
	}

	public void paintComponent(Graphics gr)
	{
		super.paintComponent(gr);

		Graphics2D g = (Graphics2D)gr;

		g.translate(-viewX, -viewY);

		for(Platform p : world.platforms)
		{
			/*g.setStroke(new BasicStroke(5));
			g.setColor(Color.RED);
			if(Methods.intersects(player,p))
				g.drawPolygon(p.getPolygon());
			g.setColor(Color.BLACK);
			g.setStroke(new BasicStroke(1));

			if(Methods.intersects(player.getPolygon(),p.getPolygon()))
				g.fillPolygon(p.getPolygon());
			else
				g.drawPolygon(p.getPolygon());*/g.drawImage(p.paint(),p.x,p.y,this);
		}
		for(Enemy p : world.enemies)
			/*g.drawPolygon(p.getPolygon());*/g.drawImage(p.paint(),p.x,p.y,this);

		g.drawImage(player.paint(), player.x, player.y, this);
		//g.drawRect(player.x,player.y,player.w, player.h);

		g.translate(viewX, viewY);

		g.drawString(""+Timer.getFPS("Painter"), 0,20);
	}

	public void keyPressed(KeyEvent evt)
	{
		down.add(evt.getKeyCode());
	}

	public void keyReleased(KeyEvent evt)
	{
		down.remove(evt.getKeyCode());
	}

	public void keyTyped(KeyEvent evt){}

	public void run()
	{
		while(true)
		{
			try
			{

				Thread.sleep(10);

				update();
				repaint();
			}catch(Exception e){e.printStackTrace();}
			System.out.print(".");
		}
	}

	public void update()
	{
		viewX += (viewXTo-viewX)/20;
		viewY += (viewYTo-viewY)/20;

		//System.out.print("A");
		player.update();

		//System.out.print("B");
		player.react(down);
		player.landed = -1;

		for(Platform p : world.platforms)
			if(Methods.intersects(p, player))
				p.support(player);

		//System.out.print("C");
		viewXTo = player.x-PlatformDriver.SCREEN.width/2 + Methods.m().x/2;
		viewYTo = (int)Math.floor((double)player.y/(PlatformDriver.SCREEN.height/2))*
			PlatformDriver.SCREEN.height/2-PlatformDriver.SCREEN.height/2+ Methods.m().y/3;
		//viewY = player.y-PlatformDriver.SCREEN.height/2;

		//System.out.print("D");
		for(Enemy e : world.enemies)
		{
			e.update();
			e.landed = -1;
			for(Platform p : world.platforms)
				if(Methods.intersects(p, e))
					p.support(e);

			//System.out.print("~");

			if(Methods.intersects(player, e))
				e.interact(player);
		}
		//System.out.print("E");
	}

	public static void main(String[] args)
	{
		PlatformDriver.main(args);
	}
}