import javax.swing.*;
import java.awt.*;
import network.*;
import java.awt.event.*;
import java.net.InetAddress;
import java.util.*;
import java.awt.geom.Point2D;

class Main extends JFrame implements NetworkListener, MouseListener, MouseMotionListener, Runnable
{
	public double E = 1.0;
	boolean colide = false;

	int id = Integer.parseInt(JOptionPane.showInputDialog("YOUR CODE")), enemyID = -1;
	boolean down, enemyDown;
	LANP2PNetwork network;
	Point2D.Double lastPos = new Point2D.Double(0,0), lastEnemyPos = new Point2D.Double(0,0),
		pos = new Point2D.Double(300,20), enemyPos = new Point2D.Double(300,750),
		puckPos = new Point2D.Double(300,200), puckVelocity = new Point2D.Double(1,1);

	double mass = 2, massOther = 2, massPuck = .5;

	LinkedList<Point> myBlur = new LinkedList<Point>();
	LinkedList<Point> enemyBlur = new LinkedList<Point>();

	long lastTime;

	Main()
	{
			try {
 			network = new LANP2PNetwork(1337, InetAddress.getByName("230.0.0.1"));
 			network.addNetworkListener(this);
		}catch(Exception e){e.printStackTrace();}

		addMouseListener(this);
		addMouseMotionListener(this);

		new Thread(this).start();
		new Thread(new MoveThread()).start();
	}

	public void receive(NetworkEvent evt)
	{
		String message = new String(evt.getData()).trim();

		String[] parts = message.split(":");

		if(enemyID < 0)
			enemyID = Integer.parseInt(parts[0]);
		else if(!parts[0].equals(""+enemyID))
			return;

		enemyPos.x = Double.parseDouble(parts[1]);
		enemyPos.y = Double.parseDouble(parts[2]);
		puckPos.x = Double.parseDouble(parts[3]);
		puckPos.y = Double.parseDouble(parts[4]);
		enemyDown = Integer.parseInt(parts[5]) == 0;
		repaint();
	}

	public void run()
	{
		while(true)
		{
			try
			{
				if(dist(puckPos,pos)<50 && !colide)
				{
					//puckVelocity.x = (int)(((pos.x-lastPos.x)*mass+puckVelocity.x*massPuck)/(massPuck*20));
					//puckVelocity.y = (int)(((pos.x-lastPos.y)*mass+puckVelocity.y*massPuck)/(massPuck*20));
					collision(pos,new Point2D.Double((pos.x-lastPos.x)/20,(pos.y-lastPos.y)/20),mass);
					colide = true;
				}
				else if(dist(puckPos,enemyPos)<50 && !colide)
				{
					//puckVelocity.x = ((enemyPos.x-lastEnemyPos.x)+puckVelocity.x)/20;
					//puckVelocity.y = ((enemyPos.x-lastEnemyPos.y)+puckVelocity.y)/20;
					collision(enemyPos,new Point2D.Double((enemyPos.x-lastEnemyPos.x),
						(enemyPos.y-lastEnemyPos.y)),massOther);
					colide = true;
				}
				else
					colide = false;


				repaint();
			}
			catch(Exception e){
				e.printStackTrace();}
		}
	}

	public void collision(Point2D.Double p, Point2D.Double v,double m)
	{
		// ball1 = thing, ball2 = puck
		double dx = puckPos.x-p.x, dy = puckPos.y-p.y;	// where x1,y1 are center of ball1, and x2,y2 are center of ball2
		double distance = Math.sqrt(dx*dx+dy*dy);	// Unit vector in the direction of the collision
		double ax=dx/distance, ay=dy/distance;		// Projection of the velocities in these axes
		double va1=(v.x*ax+v.y*ay), vb1=(-v.x*ay+v.y*ax);
		double va2=(puckVelocity.x*ax+puckVelocity.y*ay), vb2=(-puckVelocity.x*ay+puckVelocity.y*ax);
			// New velocities in these axes (after collision): E<=1,  for elastic collision E=1
		double vaP1=va1 + (1+E)*(va2-va1)/(1+m/massPuck);
		double vaP2=va2 + (1+E)*(va1-va2)/(1+massPuck/m);		// Undo the projections
		//vx1=vaP1*ax-vb1*ay;  vy1=vaP1*ay+vb1*ax;// new vx,vy for ball 1 after collision

		//vx2=vaP2*ax-vb2*ay;  vy2=vaP2*ay+vb2*ax;// new vx,vy for ball 2 after collision

		puckVelocity.x = (vaP2*ax-vb2*ay);
		puckVelocity.y = (vaP2*ay+vb2*ax);

 		/*// displacement from i to j
        double y = (p.y - puckPos.y);
        double x = (p.x - puckPos.x);

        // distance squared
        double d2 = x * x + y * y;

        // dividing by 0 is bad
        if (d2 == 0)
            return false;

        // if d^2 < (30)^2, the disks have collided
        if (dis(p,puckPos)<=50) {
            double kii, kji, kij, kjj;

            kji = (x * puckVelocity.x + y * i.dy) / d2; // k of j due to i
            kii = (x * puckVelocity.y - y * i.dx) / d2; // k of i due to i
            kij = (x * v.x + y * j.dy) / d2; // k of i due to j
            kjj = (x * v.y - y * j.dx) / d2; // k of j due to j

            // set velocity of i
            i.dy = kij * y + kii * x;
            i.dx = kij * x - kii * y;

            // set velocity of j
            j.dy = kji * y + kjj * x;
            j.dx = kji * x - kjj * y;

            return true;
        }*/

	}

	public double dist(Point2D.Double a, Point2D.Double b)
	{
		return Math.sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
	}

	public void mouseExited(MouseEvent evt){}
	public void mouseEntered(MouseEvent evt){}
	public void mouseReleased(MouseEvent evt)
	{
		down = false;
		repaint();
		send();
	}
	public void mouseClicked(MouseEvent evt){}
	public void mousePressed(MouseEvent evt)
	{
		down = true;
		send();
		repaint();
	}

	public void mouseMoved(MouseEvent evt)
	{
		down = false;
		pos.x = evt.getX();
		pos.y = evt.getY();
		send();
 		repaint();
	}
	public void mouseDragged(MouseEvent evt)
	{
		down = true;
		pos.x = evt.getX();
		pos.y = evt.getY();
		send();
 		repaint();
	}

	public void send()
	{
		//network.send((id+":"+pos.x+":"+(800-pos.y)+":"+puckPos.x+":"+puckPos.y+":"+(down?0:1)).getBytes());
	}

	public void paint(Graphics ga)
	{
		if(getBufferStrategy() == null)
			createBufferStrategy(2);

		if(System.currentTimeMillis()-lastTime > 40000)
		{
			myBlur.addFirst(new Point((int)pos.x,(int)pos.y));

			if(myBlur.size()>20)
				myBlur.removeLast();

			enemyBlur.addFirst(new Point((int)enemyPos.x,(int)enemyPos.y));

			if(enemyBlur.size()>20)
				enemyBlur.removeLast();
		}

		Graphics g = getBufferStrategy().getDrawGraphics();

		super.paint(g);

		for(int i = 0; i < myBlur.size(); i++)
		{
			Point p = myBlur.get(i);
			Color c = Methods.randomColor(id,down?255:255-10*i);
			g.setColor(new Color((int)(c.getRed()+(getBackground().getRed()-c.getRed())*(double)i/20),
								(int)(c.getGreen()+(getBackground().getGreen()-c.getGreen())*(double)i/20),
								(int)(c.getBlue()+(getBackground().getBlue()-c.getBlue())*(double)i/20)));
			g.fillOval(p.x-30,p.y-30,60,60);
		}

		for(int i = 0; i < enemyBlur.size(); i++)
		{
			Point p = enemyBlur.get(i);
			Color c = Methods.randomColor(enemyID,enemyDown?255:255-10*i);
			g.setColor(new Color((int)(c.getRed()+(getBackground().getRed()-c.getRed())*(double)i/20),
								(int)(c.getGreen()+(getBackground().getGreen()-c.getGreen())*(double)i/20),
								(int)(c.getBlue()+(getBackground().getBlue()-c.getBlue())*(double)i/20)));
			g.fillOval(p.x-30,p.y-30,60,60);
		}


		g.setColor(Color.RED);
		g.fillOval((int)(puckPos.x-20),(int)(puckPos.y-20),40,40);

		g.setColor(Methods.randomColor(enemyID,enemyDown?255:100));
		g.fillOval((int)(enemyPos.x-30),(int)(enemyPos.y-30),60,60);

		g.setColor(Methods.randomColor(id,down?255:100));
		g.fillOval((int)(pos.x-30),(int)(pos.y-30),60,60);


		getBufferStrategy().show();
	}

	private class MoveThread implements Runnable
	{
		public void run()
		{
			while(true)
			{
				try
				{
					Thread.sleep(10);
				}
				catch(Exception e){}

				lastPos.x = pos.x;
				lastPos.y = pos.y;
				lastEnemyPos.x = enemyPos.x;
				lastEnemyPos.y = enemyPos.y;

				puckVelocity.x *= .9999;
				puckVelocity.y *= .9999;

				puckPos.x += puckVelocity.x;
				puckPos.y += puckVelocity.y;

				if(puckPos.x-30<=0||puckPos.x+30>=600)
					puckVelocity.x*=-1;
				if(puckPos.y-30<=0||puckPos.y+30>=800)
					puckVelocity.y*=-1;
			}
		}
	}

	public static void main(String[] args)
	{
		JFrame frame =  new Main();
		Methods.centerFrame(600,800,frame);
		frame.setVisible(true);
	}
}