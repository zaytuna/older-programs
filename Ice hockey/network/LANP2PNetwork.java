/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Clement
 */
public class LANP2PNetwork extends Network {

    ArrayList<InetAddress> clients = new ArrayList<InetAddress>();
    ArrayList<MulticastSocket> senders = new ArrayList<MulticastSocket>();
    ArrayList<MulticastSocket> receivers = new ArrayList<MulticastSocket>();
    InetAddress group;
    protected MulticastSocket sendLAN;
    protected MulticastSocket receiveLAN;

    /**
     * Creates a new P2P (peer to peer) network over a LAN. This class is
     * unsuitable for Internet use.
     * @param port the port that data should be sent through
     * @param group the IP
     */
    public LANP2PNetwork(int port, InetAddress group) {
        super(port);
        try {
            sendLAN = new MulticastSocket(port);
            this.group = group;
            receiveLAN = new MulticastSocket(port);
            sendLAN.joinGroup(group);
            receiveLAN.joinGroup(group);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startReceiving();
    }

    public void addClient(InetAddress ip, MulticastSocket receive,
            MulticastSocket send) {
        clients.add(ip);
        senders.add(send);
        receivers.add(receive);
        try {
            receive.joinGroup(group);
            send.joinGroup(group);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void kick(InetAddress ip, MulticastSocket receive,
            MulticastSocket send) {
        clients.remove(ip);
        senders.remove(send);
        receivers.remove(receive);
        try {
            receiveLAN.leaveGroup(group);
            sendLAN.leaveGroup(group);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void send(NetworkObject no) {
        send(no.getBytes());
    }

    @Override
    public void send(byte[] bytes) {
        DatagramPacket packet = new DatagramPacket(bytes,
                bytes.length, group, port);
        try {
            sendLAN.send(packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public DatagramSocket getSendSocket() {
        return sendLAN;
    }

    @Override
    public DatagramSocket getReceiveSocket() {
        return receiveLAN;
    }

    @Override
    public void close() {
        clients.clear();
    }

    @Override
    public void changePort(int i) {
        try {
            send = new MulticastSocket(i);
            receive = new MulticastSocket(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
        port = i;
    }
}
