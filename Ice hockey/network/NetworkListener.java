/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.util.EventListener;

/**
 *
 * @author Clement
 */
public interface NetworkListener extends EventListener {

    public void receive(NetworkEvent e);
}
