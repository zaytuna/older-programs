/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Clement
 */
public class NetworkReceiver implements Runnable {

    protected boolean receive, die;
    protected DatagramSocket receiver;
    protected ArrayList<Byte[]> data = new ArrayList<Byte[]>();
    protected int inputBufferSize;
    protected final ReentrantLock LOCK = new ReentrantLock();
    protected Network network;
    protected byte[] temp;

    public NetworkReceiver(DatagramSocket receiver, int inputBufferSize,
            Network n) {
        receive = true;
        die = false;
        this.receiver = receiver;
        this.inputBufferSize = inputBufferSize;
        network = n;
    }

    public void run() {
        while (!die) {
            if (receive) {
                byte[] inputBuffer = new byte[inputBufferSize];
                DatagramPacket incomingData =
                        new DatagramPacket(inputBuffer, inputBufferSize);
                try {
                    receiver.receive(incomingData);
                    temp = incomingData.getData();
                    LOCK.lock();
                    data.add(box(temp));
                    LOCK.unlock();
                    network.fireNetworkEvent(new NetworkEvent(this, temp));
                } catch (Exception e) {
                    Logger.getLogger(LANP2PNetwork.class.getName()).
                            log(Level.SEVERE, null, e);
                }
            }
            Thread.yield();
        }
    }

    public void clear() {
        LOCK.lock();
        data.clear();
        LOCK.unlock();
    }

    public Byte[] getData(int index) {
        LOCK.lock();
        Byte[] a = data.get(index);
        LOCK.unlock();
        return a;

    }

    private Byte[] box(byte[] array) {
        int num = array.length;
        Byte[] boxed = new Byte[num];
        for (int ct = 0; ct < num; ++ct) {
            boxed[ct] = Byte.valueOf(array[ct]);
        }
        return boxed;
    }

    private byte[] box(Byte[] array) {
        int num = array.length;
        byte[] boxed = new byte[num];
        for (int ct = 0; ct < num; ++ct) {
            boxed[ct] = (byte) array[ct];
        }
        return boxed;
    }

    public void stopReceiving() {
        receive = false;
    }

    public void startReceiving() {
        receive = true;
    }

    public void kill() {
        die = true;
    }
}
