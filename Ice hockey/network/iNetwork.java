/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.net.InetAddress;

/**
 *
 * @author Clement
 */
public abstract class iNetwork extends Network {

    public iNetwork(int port) {
        super(port);
    }

    public abstract void addClient(InetAddress ip);
    public abstract void kick(InetAddress ip);
}
