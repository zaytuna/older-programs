/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package network;

import java.util.EventObject;

/**
 *
 * @author Clement
 */
public class NetworkEvent extends EventObject {

    protected byte[] data;
    protected long timeStamp;

    /**
     * Constructs a new NetworkEvent.
     * @see NetworkListener
     * @param source the receiver of the NetworkEvent
     * @param sent the data that was sent
     */
    public NetworkEvent(Object source, byte[] sent) {
        super(source);
        data = sent;
        timeStamp = System.currentTimeMillis();
    }

    /**
     * Gets the sent data
     * @return data a byte array containing the data that was sent.
     */
    public byte[] getData() {
        return data;
    }

    /**
     * This gets the time in millis. The related call is
     * System.currentTimeMillis().
     * @return timeStamp the time in millis when the event was created,
     * not necessarily sent time or when the event was fired
     */
    public long getTime() {
        return timeStamp;
    }

}
