/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Clement
 */
public class InetP2PNetwork extends iNetwork {

    protected ArrayList<InetAddress> clients = new ArrayList<InetAddress>();

    public InetP2PNetwork(int port, InetAddress group) {
        super(port);
        try {
            send = new DatagramSocket(port);
            receive = new DatagramSocket(port);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startReceiving();
    }

    @Override
    public void send(NetworkObject no) {
        send(no.getBytes());
    }

    @Override
    public void send(byte[] bytes) {
        try {
            for (InetAddress a : clients) {
                if (!a.equals(InetAddress.getLocalHost())) {
                    DatagramPacket packet = new DatagramPacket(bytes, bytes.length,
                            a, port);

                    send.send(packet);

                }
            }
        } catch (Exception e) {
            Logger.getLogger(InetP2PNetwork.class.getName()).
                    log(Level.SEVERE, null, e);
        }
    }

    @Override
    public DatagramSocket getSendSocket() {
        return send;
    }

    @Override
    public DatagramSocket getReceiveSocket() {
        return receive;
    }

    @Override
    public void addClient(InetAddress ip) {
        clients.add(ip);
    }

    @Override
    public void kick(InetAddress ip) {
        clients.remove(ip);
    }

    @Override
    public void close() {
        clients.clear();
    }

    @Override
    public void changePort(int i) {
        try {
            send = new DatagramSocket(i);
            receive = new DatagramSocket(i);
        } catch (Exception e) {
            Logger.getLogger(LANP2PNetwork.class.getName()).
                    log(Level.SEVERE, null, e);
        }
        port = i;
    }
}
