/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package network;

/**
 *
 * @author Clement
 */
public interface NetworkObject {
    
    public byte[] getBytes();
    public NetworkObject retrieveFromBytes(Byte[] bytes);
    public int length();
}
