/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author Clement
 */
public abstract class Network {

    private List<NetworkListener> l = new ArrayList<NetworkListener>();
    protected int port;
    protected DatagramSocket send;
    protected DatagramSocket receive;
    protected NetworkReceiver nr;

    protected Network(int port) {
        this.port = port;
    }

    /**
     * Adds a NetworkListener to its list so that NetworkEvents can be fired.
     * @param nl the NetworkListener to be added
     */
    public void addNetworkListener(NetworkListener nl) {
        l.add(nl);
    }

    /**
     * With adding capability comes remove.
     * @param nl the NetworkListener to be removed.
     */
    public void removeNetworkListener(NetworkListener nl) {
        l.remove(nl);
    }

    /**
     * Fires a NetworkEvent by calling every NetworkListener's receive method
     * @param e the NetworkEvent to be fired.
     */
    public void fireNetworkEvent(NetworkEvent e) {
        for (NetworkListener nl : l) {
            nl.receive(e);
        }
    }

    /**
     * Starts the receiving thread and initializes the Receiver.
     * Note: this method is intended just to make the subclass' job easier.
     * The subclass needs to call this AFTER initializing the sockets.
     */
    public void startReceiving() {
        nr = new NetworkReceiver(getReceiveSocket(), 256, this);
        ExecutorService es = Executors.newCachedThreadPool();
        es.execute(nr);
        es.shutdown();
    }

    /**
     * Retrieves the sending socket
     * @return send the DatagramSocket that is in charge of sending stuff
     */
    public abstract DatagramSocket getSendSocket();

    /**
     * Retrieves the receiving socket
     * @return send the DatagramSocket that is in charge of receiving stuff
     */
    public abstract DatagramSocket getReceiveSocket();

    /**
     * Sends an object. Since different types of networking may involve
     * different methods of sending, this is left as a required method.
     */
    public abstract void send(NetworkObject no);

    /**
     * Sends an array of bytes. Since different types of networking may involve
     * different methods of sending, this is left as a required method.
     */
    public abstract void send(byte[] bytes);

    /**
     * Closes connections.
     */
    public abstract void close();

    /**
     * Changes the port.
     * Note: this may require reinitializing of the sockets.
     */
    public abstract void changePort(int i);
}
