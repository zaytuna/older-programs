/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package network;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Clement
 */
public class ServerClientNetwork extends iNetwork {

    InetAddress server;
    ArrayList<InetAddress> clients = new ArrayList<InetAddress>();

    public ServerClientNetwork(int port, InetAddress server) {
        super(port);
        try {
            send = new DatagramSocket(port);
            receive = new DatagramSocket(port);
        } catch (Exception e) {
            Logger.getLogger(LANP2PNetwork.class.getName()).
                    log(Level.SEVERE, null, e);
        }
        startReceiving();
    }

    @Override
    public void addClient(InetAddress client) {
        clients.add(client);
    }

    @Override
    public void kick(InetAddress client) {
        if(!client.equals(server)) {
            clients.remove(client);
        }
        else {
            clients.clear();
        }
    }

    @Override
    public void send(NetworkObject no) {
        send(no.getBytes());
    }

    @Override
    public void send(byte[] bytes) {
        try {
            if (!InetAddress.getLocalHost().equals(server)) {
                DatagramPacket packet = new DatagramPacket(bytes,
                        bytes.length, server, port);
                send.send(packet);
            } else {
                for (InetAddress ip : clients) {
                    DatagramPacket packet = new DatagramPacket(bytes,
                            bytes.length, ip, port);
                    send.send(packet);
                }

            }
        } catch (Exception e) {
            Logger.getLogger(LANP2PNetwork.class.getName()).
                    log(Level.SEVERE, null, e);
        }
    }

    @Override
    public DatagramSocket getSendSocket() {
        return send;
    }

    @Override
    public DatagramSocket getReceiveSocket() {
        return receive;
    }

    @Override
    public void close() {
        try {
            kick(InetAddress.getLocalHost());
        } catch (UnknownHostException ex) {
            Logger.getLogger(LANP2PNetwork.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void changePort(int i) {
        try {
            send = new DatagramSocket(i);
            receive = new DatagramSocket(i);
        }catch(Exception e) {e.printStackTrace();}
        port = i;
    }
}
