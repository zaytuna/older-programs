class Projectile
{
	java.awt.Color color;
	java.awt.Point position;
	int vx,vy;
	int ax,ay;
	double damage;
	double range = 40;
	int behavior;// 0 == go on straight path; 1 == home in on target; 2 == random curving around;
	int specialness;
	Enemy target;
	Tower owner;

	Projectile(Tower owner)
	{
		behavior = owner.mode;
		damage = owner.damage;
		color = owner.color;
		this.specialness = owner.specialness;
		this.owner = owner;

		position = new java.awt.Point(owner.position);
	}
	public void update(World2D world)
	{
		if(behavior == 1)
		{
			double distance = Math.sqrt((target.position.x-position.x)*(target.position.x-position.x)+
				(target.position.y-position.y)*(target.position.y-position.y));

			position.x += velocity()*(target.position.x-position.x)/distance;
			position.y += velocity()*(target.position.x-position.x)/distance;
			//System.out.println(velocity());
			if(distance<range)
			{
				target.takeDamage(damage);
				if(target.health<=0)
					owner.kills++;
				world.queueRemove(this);
			}
			vx += ax;
			vy += ay;
		}
		else if(behavior == 0)
		{
			position.x += vx;
			position.y += vy;
			for(Enemy e:world.enemies)
				if(distance(position,e.position)<range)
				{
					e.takeDamage(damage);
					if(e.health<=0)
						owner.kills++;
					if(specialness!=3)
						world.queueRemove(this);
				}

			if(specialness!=3)
			{
				//vx += ax;
			//	vy += ay;
			}
		}


		if(position.x<0||position.y<0||position.x>GameDriver.screen.width||
			position.y>GameDriver.screen.height)
			world.queueRemove(this);

	}
	public double velocity()
	{
		return Math.sqrt(vx*vx+vy*vy);
	}
	public static double distance(java.awt.Point p, java.awt.Point p2)
	{
		return Math.sqrt((p.x-p2.x)*(p.x-p2.x)+(p.y-p2.y)*(p.y-p2.y));
	}
}