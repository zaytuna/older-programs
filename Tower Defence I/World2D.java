import java.util.*;
import java.awt.*;

class World2D
{
	ArrayList<Tower> towers = new ArrayList<Tower>();
	ArrayList<Projectile> projectiles = new ArrayList<Projectile>();
	ArrayList<Enemy> enemies = new ArrayList<Enemy>();

	Set<Projectile> removeP = new HashSet<Projectile>();
	Set<Enemy> removeE = new HashSet<Enemy>();
	Set<Tower> removeT = new HashSet<Tower>();
	boolean remove = false;

	public void addTower(Tower t)
	{
		towers.add(t);
		t.world = this;
	}
	public void queueRemove(Projectile j)
	{
		removeP.add(j);
		remove = true;
	}
	public void queueRemove(Enemy j)
	{
		removeE.add(j);
		remove = true;
	}
	public void queueRemove(Tower j)
	{
		removeT.add(j);
		remove = true;
	}
	public synchronized void update()
	{
		for(Tower t:towers)t.update();
		for(Projectile j: projectiles)j.update(this);
		for(Enemy e:enemies)e.update(this);

		if(remove)
		{
			for(Projectile j: removeP)
				projectiles.remove(j);
			for(Enemy e: removeE)
				enemies.remove(e);
			for(Tower t:removeT)
				towers.remove(t);
		}

		remove = false;
	}
	public void createBlackHole(Point p)
	{

	}
	public void clearAll()
	{
		towers.clear();
		projectiles.clear();
		enemies.clear();
		removeT.clear();
		removeE.clear();
		removeP.clear();
	}
	public ArrayList<Point> findPath(Point from,Point to)
	{
		ArrayList<Point> path = new ArrayList<Point>();

		path.add(to);
		return path;
	}
}