import java.awt.*;
import java.util.ArrayList;

class Enemy
{
	Point position;
	int health;
	int maxHealth;
	int armor = 1;
	double speed = 1;
	Point to;

	boolean poisoned;
	boolean slowed;

	ArrayList<Point> path = new ArrayList<Point>();

	public void update(World2D world)
	{
		if(path.size()==0)
		{
			path.add(to);
		}
		double length = Math.sqrt((path.get(0).x-position.x)*(path.get(0).x-position.x)+
			(path.get(0).y-position.y)*(path.get(0).y-position.y));

		for(Tower t: world.towers)
		{
			if(new Rectangle(position.x-20,position.y-20,40,40).contains(t.position))
				path = world.findPath(position,path.get(0));
		}

		position.x += speed*((path.get(0).x-position.x)*5)/length;
		position.y += speed*((path.get(0).y-position.y)*5)/length;

		if(Math.abs(path.get(0).x-position.x)<speed&& Math.abs(path.get(0).y-position.y)<speed)
		{
			System.out.println(path.size());
			if(path.size()>=2)
				path.remove(0);
			else
				reachDestination(world);
		}

		if(health <= 0)
		{
			world.queueRemove(this);
			GameDriver.moneys += Math.sqrt(maxHealth);
			world.queueRemove(this);
		}
	}
	public void takeDamage(double amt)
	{
		health -= amt/armor;
	}
	private void reachDestination(World2D w)
	{
		System.out.println("Yipee!");

		GameDriver.health -= health;
		w.queueRemove(this);
	}
}