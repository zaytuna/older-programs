import java.awt.*;
import java.util.*;
import java.io.*;

class Methods
{
	public static void centerFrame(int frameWidth, int frameHeight, Component c)
	{
		Toolkit tools = Toolkit.getDefaultToolkit();
		Dimension screen = tools.getScreenSize();
		int xUpperLeftCorner = (screen.width-frameWidth)/2;
		int yUpperLeftCorner = (screen.height-frameHeight)/2;

		c.setBounds(xUpperLeftCorner,yUpperLeftCorner,frameWidth,frameHeight);
	}
	public static <T> void shuffle(T[] stuff)
	{
		for(int i = 0; i < stuff.length*10; i++)
			swap(stuff,(int)(Math.random()*stuff.length),(int)(Math.random()*stuff.length));
	}
	public static void shuffle(int[] stuff)
	{
		for(int i = 0; i < stuff.length*10; i++)
			swap(stuff,(int)(Math.random()*stuff.length),(int)(Math.random()*stuff.length));
	}
	public static <T> void swap(T[] stuff, int posA,int posB)
	{
		T inA = stuff[posA];
		stuff[posA] = stuff[posB];
		stuff[posB] = inA;
	}
	public static Color colorMeld(Color a, Color b, double ratio)
	{
		return new Color((int)(a.getRed()+(b.getRed()-a.getRed())*ratio),(int)(a.getGreen()+(b.getGreen()-a.getGreen())*ratio),
			(int)(a.getBlue()+(b.getBlue()-a.getBlue())*ratio));
	}
	public static Point mouse()
	{
		return MouseInfo.getPointerInfo().getLocation();
	}
	public static void swap(int[] stuff, int posA,int posB)
	{
		int inA = stuff[posA];
		stuff[posA] = stuff[posB];
		stuff[posB] = inA;
	}

	public static double pseudoRandom(int x)
	{
		x = (x<<13) ^ x;
 		return (1.0f - ((x * (x*x * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0d);
	}
	public static Color randomColor()
	{
		return new Color((int)(Math.random()*255),(int)(Math.random()*255),
			(int)(Math.random()*255),250);
	}
	public static Color randomColor(int seed, int alpha)
	{
		return new Color((int)Math.abs(pseudoRandom(seed)*255),(int)Math.abs(pseudoRandom(seed+52)*255),
			(int)Math.abs(pseudoRandom(seed*5)*255),alpha<0?(int)Math.abs(pseudoRandom(seed*29+17)*255):alpha);
	}
	public static <T> T[] createArray(ArrayList<T> ls, T[] ar)
	{
		for(int i = 0; i < ls.size(); i++)
			ar[i] = ls.get(i);
		return ar;
	}
	public static Object[] createObjectArray(ArrayList<? extends Object> ls, Object[] ar)
	{
		for(int i = 0; i < ls.size(); i++)
			ar[i] = ls.get(i);
		return ar;
	}
	public static <T> ArrayList<T> getList(T[] from)
	{
		ArrayList<T> list = new ArrayList<T>();

		for(T t: from)
			list.add(t);

		return list;
	}
	public static double max(double[][] array)
	{
		double max = array[0][0];
		for(double[] d:array)
			for(double number:d)
				if(number>max)
					max = number;

		return max;
	}
	public static double getMax(Collection<? extends Number> nums)
	{
		double max = nums.iterator().next().doubleValue();
		for(Number n:nums)
			if(n.doubleValue()>max)
				max = n.doubleValue();
		return max;

	}
	public static int getMaxLength(Map... maps)
	{
		int max = 0;
		for(Map map: maps)
			if(map.keySet().size()>max)
				max = map.keySet().size();
		return max;
	}
	public static double sum(double[] array)
	{
		double sum = 0;
		for(double d:array)
			sum += d;
		return sum;
	}
	public static String getFileContents(File f)
	{
		try
		{
			StringBuilder sb = new StringBuilder();

			BufferedReader in = new BufferedReader(new FileReader(f));
			while(true)
			{
				String temp = in.readLine();
				if(temp == null)
					break;
				sb.append(temp+"\n");
			}
			in.close();
			return sb.toString();
		}
		catch(Exception e){e.printStackTrace();}
		return null;
	}
}