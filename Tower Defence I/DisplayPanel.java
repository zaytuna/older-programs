import java.awt.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.ArrayList;

class DisplayPanel extends JPanel implements MouseListener, MouseMotionListener, Runnable, ActionListener
{
	World2D world;
	int viewx, viewy;
	Tower nextTower;
	Tower[] towers;
	int selected = -1;
	Color selectColor = new Color(255,200,0,100);
	int selectDist = 10;

	JButton meld = new JButton("Meld");
	JButton next = new JButton("Next");
	JButton upgradeA = new JButton("Attack");
	JButton upgradeR = new JButton("Range");
	JButton upgradeS = new JButton("Cooldown");
	JButton upgradeSi = new JButton("Size");
	JButton sell = new JButton("Sell");

	Point start,end;

	ArrayList<Tower> selection = new ArrayList<Tower>();

	DisplayPanel(World2D w)
	{
		super(true);
		setLayout(null);
		world = w;
		setBackground(Color.WHITE);
		addMouseListener(this);

		addMouseMotionListener(this);
		towers = new Tower[]{
			new Tower(3,10,Color.RED,1,30,50,0,0,world,1),
			new Tower(5,15,Color.ORANGE,1,10,70,0,0,world,10),
			new Tower(100,100, Color.YELLOW,1,100,200,0,0,world,10),
			new Tower(50,35, Color.GREEN,1,50,120,0,0,world,20),
			new Tower(1,18,Color.BLUE,1,2,70,0,0,world,90),
			new Tower(1000,120,Color.BLACK,1,100,1000,3,0,world,100)
		};
		for(Tower t: towers)
			t.setCost();
	}
	public void setBounds(int a, int b, int c, int d)
	{
		super.setBounds(a,b,c,d);
		init(c,d);
	}
	public void actionPerformed(ActionEvent evt)
	{
		if(evt.getSource() == sell)
		{
			for(Tower t: selection)
			{
				GameDriver.moneys+=t.cost;
				world.queueRemove(t);
			}
		}
		else if(evt.getSource() == meld)
		{
			double xAvg = 0;
			double yAvg = 0;

			for(Tower t: selection)
			{
				xAvg += t.position.x;
				yAvg += t.position.y;
			}

			xAvg/=selection.size();
			yAvg/=selection.size();

			for(Tower t: selection)
			{
				t.to = new Point((int)xAvg,(int)yAvg);
				t.move = true;
			}

		}
		else if(evt.getSource() == upgradeA)
		{
			for(Tower t: selection)
			{
				int cost = t.cost;
				t.damage *= 1.5;
				t.setCost();
				GameDriver.moneys -= (t.cost-cost);

				if(GameDriver.moneys<0)
				{
					GameDriver.moneys+=(t.cost-cost);
					t.damage /= 1.1;
					t.setCost();
				}
			}
		}
		else if(evt.getSource() == upgradeR)
		{
			for(Tower t: selection)
			{
				int cost = t.cost;
				t.range += 30;
				t.setCost();
				GameDriver.moneys -= (t.cost-cost);

				if(GameDriver.moneys<0)
				{
					GameDriver.moneys+=(t.cost-cost);
					t.range-=30;
					t.setCost();
				}
			}
		}
		else if(evt.getSource() == upgradeS)
		{
			for(Tower t: selection)
			{
				int cost = t.cost;
				t.reload /= 1.5;
				t.setCost();
				GameDriver.moneys -= (t.cost-cost);

				if(GameDriver.moneys<0)
				{
					GameDriver.moneys+=(t.cost-cost);
					t.reload*=1.5;
					t.setCost();
				}
			}
		}
		else if(evt.getSource() == upgradeSi)
		{
			for(Tower t: selection)
			{
				int cost = t.cost;
				t.size += 10;
				t.setCost();
				GameDriver.moneys -= 10+(t.cost-cost);

				if(GameDriver.moneys<0)
				{
					GameDriver.moneys += 10+(t.cost-cost);
					t.size-=10;
					t.setCost();
				}
			}
		}
	}
	public void run()
	{
	}
	private void init(int w, int h)
	{
		//next.setBounds();

		sell.setBounds(w-70,h-25,70,25);
		meld.setBounds(w-70,h-50,70,25);
		upgradeA.setBounds(w-195,h-50,90,25);
		upgradeR.setBounds(w-195,h-25,90,25);
		upgradeS.setBounds(w-195,h-100,90,25);
		upgradeSi.setBounds(w-195,h-75,90,25);

		add(next);
		add(sell);
		add(meld);
		add(upgradeA);
		add(upgradeS);
		add(upgradeSi);
		add(upgradeR);

		sell.addActionListener(this);
		meld.addActionListener(this);
		upgradeA.addActionListener(this);
		upgradeR.addActionListener(this);
		upgradeS.addActionListener(this);
		upgradeSi.addActionListener(this);
	}

	public void mouseMoved(MouseEvent evt)
	{
	}
	public void mouseDragged(MouseEvent evt)
	{
		if(nextTower==null)
		{
			end = evt.getPoint();
		}
		repaint();
	}
	public void mousePressed(MouseEvent evt)
	{
		if(nextTower==null)start = evt.getPoint();
		repaint();
	}
	public void mouseReleased(MouseEvent evt)
	{
		selection.clear();

		if(start!=null&&end!=null)
		{
			Rectangle area = new Rectangle(Math.min(start.x,end.x),Math.min(start.y,end.y),Math.abs(end.x-start.x),
				Math.abs(end.y-start.y));

			for(Tower t:world.towers)
			{
				if(area.contains(t.position))
					selection.add(t);
			}
		}

		start = null;
		end = null;

		if(new Rectangle(getWidth()-301,getHeight()-31,100,30).contains(evt.getPoint()))
		{
			GameDriver.wait = 2;
			return;
		}

		if((nextTower != null) && !(new Rectangle(getWidth()-200,10,200,getHeight()-20).contains(evt.getPoint()))
			&& (GameDriver.moneys >= nextTower.cost))
		{
			Tower t = nextTower.clone();
			t.position = evt.getPoint();
			world.addTower(t);
			GameDriver.moneys-=t.cost;
			repaint();
		}

		boolean select = false;
		for(int i = 0; i < towers.length; i++)
		{
			if(evt.getX()>(getWidth()-195+(i%5)*38) && (evt.getY()>100+(i/5)*38) &&
				evt.getX()<(getWidth()-161+(i%5)*38) && (evt.getY()<134+(i/5)*38))
			{
				selected = i;
				nextTower = towers[i];
				repaint();
				select = true;
			}
		}
		if(!select&&(new Rectangle(getWidth()-200,10,200,getHeight()-20).contains(evt.getPoint())))
		{
			selected = -1;
			nextTower = null;
		}
	}
	public void mouseEntered(MouseEvent evt){}
	public void mouseExited(MouseEvent evt){}
	public void mouseClicked(MouseEvent evt){}
	public void paintComponent(Graphics gr)
	{
		super.paintComponent(gr);

		Graphics2D g = (Graphics2D)gr;

		for(Tower t: world.towers)
		{
			g.setColor(t.color);
			g.fillOval(t.position.x-t.size/2,t.position.y-t.size/2,t.size,t.size);
			g.setColor(Color.RED);
			g.fillRect(t.position.x-t.size/2,t.position.y+t.size/2,(int)(t.size*Math.min(t.reloading,1)),10);
			g.setColor(Color.BLACK);
			g.drawRect(t.position.x-t.size/2,t.position.y+t.size/2,t.size,10);

			if(selection.contains(t))
			{
				g.setColor(selectColor);
				g.setStroke(new BasicStroke(10));
				g.drawOval(t.position.x-t.size/2-selectDist,t.position.y-t.size/2-selectDist,
					t.size+2*selectDist,t.size+2*selectDist);

				g.setStroke(new BasicStroke(1));
				g.setColor(Color.BLACK);

				g.drawOval(t.position.x-t.size/2-selectDist-5,t.position.y-t.size/2-selectDist-5,
					t.size+2*selectDist+10,t.size+2*selectDist+10);
				g.drawOval(t.position.x-t.size/2-selectDist+5,t.position.y-t.size/2-selectDist+5,
					t.size+2*selectDist-10,t.size+2*selectDist-10);

				//g.setColor(new Color(130,130,130,70));
				//g.fillOval(t.position.x-t.size/2-t.range,t.position.y-t.size/2-t.range,
				//	t.size+2*t.range,t.size+2*t.range);
				g.setColor(Color.BLACK);

				float[] dashesHi = {10};
				g.setStroke(new BasicStroke(4,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND,10,dashesHi,0));
				g.drawOval(t.position.x-t.size/2-t.range,t.position.y-t.size/2-t.range,
					t.size+2*t.range,t.size+2*t.range);
				g.setStroke(new BasicStroke(1));
			}
		}
		for(Projectile j : world.projectiles)
		{
			g.setStroke(new BasicStroke(5));
			g.setColor(j.color);
			g.drawLine(j.position.x,j.position.y,j.position.x+j.vx,j.position.y+j.vy);
		}
		for(Enemy e: world.enemies)
		{
			g.setColor(Color.BLACK);
			g.fillRect(e.position.x-20,e.position.y-20,40,40);
			g.setColor(Color.BLUE);
			g.fillRect(e.position.x-20,e.position.y+10,(40*e.health)/e.maxHealth,10);
			g.setColor(Color.WHITE);
			g.setFont(new Font("SERIF",Font.BOLD,20));
			g.drawString(""+e.health,e.position.x-20,e.position.y+10);
		}

		g.setColor(new Color(0,0,0,100));
		g.fillRoundRect(getWidth()-200,10,200,getHeight()-20,90,90);

		g.drawString("Money: "+GameDriver.moneys,getWidth()-180,30);
		g.drawString("Health: "+GameDriver.health+"/1000",getWidth()-180,50);

		for(int i = 0; i < towers.length; i++)
		{
			g.setColor(towers[i].color);
			g.fillRect(getWidth()-195+(i%5)*38,100+(i/5)*38,34,34);
			if(i == selected)
			{
				g.setColor(Color.BLACK);
				g.drawRect(getWidth()-195+(i%5)*38,100+(i/5)*38,34,34);
			}
			g.setColor(Color.BLACK);
			g.drawString(""+towers[i].cost,getWidth()-195+(i%5)*38,100+(i/5)*38);
		}
		for(int i = 0; i < selection.size(); i++)
		{
			g.setColor(selection.get(i).color);
			g.fillRect(getWidth()-195+(i%5)*38,getHeight()-150-(i/5)*38,34,34);
			g.setColor(new Color(255-selection.get(i).color.getRed(),100,100));
			g.drawString(""+selection.get(i).kills,getWidth()-195+(i%5)*38,getHeight()-130-(i/5)*38);
		}

		g.setStroke(new BasicStroke(1));

		g.setColor(new Color(255,231,155));
		g.fillRect(getWidth()-301,getHeight()-31,100,30);
		g.setColor(Color.BLACK);
		g.drawRect(getWidth()-301,getHeight()-31,100,30);
		g.setFont(new Font("SERIF",Font.BOLD,20));
		g.drawString("Next Wave",getWidth()-297,getHeight()-8);

		g.setColor(new Color(100,0,0,100));
		if(start!=null&&end!=null)
			g.fillRoundRect(Math.min(start.x,end.x),Math.min(start.y,end.y),Math.abs(end.x-start.x),
				Math.abs(end.y-start.y),30,30);
	}
}