import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

class SidePanel extends JPanel implements MouseListener
{
	DisplayPanel display;
	GameDriver owner;
	Tower[] towers;

	int selected;

	SidePanel(DisplayPanel d, GameDriver g)
	{
		setBackground(new Color(255,231,155));
		this.display = d;
		this.owner = g;

		towers = new Tower[]{
			new Tower(10,10,Color.RED,1,30,50,0,0,display.world,10),
			new Tower(5,15,Color.ORANGE,1,15,70,0,0,display.world,10),
			new Tower(100,100, Color.YELLOW,1,100,200,0,0,display.world,10),
			new Tower(50,35, Color.GREEN,1,50,120,0,0,display.world,10),
			new Tower(1,18,Color.BLUE,1,2,70,0,0,display.world,10),
			new Tower(1000,120,Color.BLACK,1,150,1000,0,0,display.world,30)
		};
		addMouseListener(this);
	}
	public void mousePressed(MouseEvent evt){}
	public void mouseEntered(MouseEvent evt){}
	public void mouseExited(MouseEvent evt){}
	public void mouseClicked(MouseEvent evt){}
	public void mouseReleased(MouseEvent evt)
	{
		System.out.println("Released at: "+evt.getPoint());
		for(int i = 0; i < towers.length; i++)
		{
			if(evt.getX()>(5+(i%5)*38) && (evt.getY()>100+(i/5)*38) &&
				evt.getX()<(39+(i%5)*38) && (evt.getY()<134+(i/5)*38))
			{
				System.out.println("Hit: "+i);
				selected = i;
				display.nextTower = towers[i];
				repaint();
			}
		}
	}

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		g.drawString("Money: "+owner.moneys,20,30);

		for(int i = 0; i < towers.length; i++)
		{
			g.setColor(towers[i].color);
			g.fillRect(5+(i%5)*38,100+(i/5)*38,34,34);
			if(i == selected)
			{
				g.setColor(Color.BLACK);
				g.drawRect(5+(i%5)*38,100+(i/5)*38,34,34);
			}
			//g.setColor(Color.BLACK);
			//g.drawString(""+towers[i].cost,(i%6)*32,((i+1)/6)*32);
		}
	}
}