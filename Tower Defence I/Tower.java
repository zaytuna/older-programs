class Tower
{
	boolean move = false;
	int damage;
	int size;
	java.awt.Color color;
	int shootType;
	int reload = 45;
	int cost = 100000;
	int range;
	int kills = 0;
	double reloading = 0;
	int specialness = 3;//0 = nothing; 1 == explode, 2 = stick, 3 = continue regardless, 4+ = num of hits can take before dying
	int mode = 0;// 0 == go on straight path; 1 == home in on target; 2 == random curving around;
	java.awt.Point position, to;
	World2D world;

	Tower(){}
	Tower(int d, int s, java.awt.Color c, int sh, int rel, int ran, int spec, int mode, World2D w, int cst)
	{
		damage = d;
		size = s;
		color = c;
		shootType = sh;
		reload = rel;
		range = ran;
		specialness = spec;
		this.mode = mode;
		world = w;
		cost = cst;
	}
	public void update()
	{
		if(move)
		{
			position.x += Math.round((to.x-position.x)/3);
			position.y += Math.round((to.y-position.y)/3);
		}

		if(reloading>=1)
			for(Enemy e:world.enemies)
				if(canAttack(e))
				{
					Projectile p = new Projectile(this);
					p.vx = (int)(60*(e.position.x-position.x)/distance(e.position,position));
					p.vy = (int)(60*(e.position.y-position.y)/distance(e.position,position));
					p.ax = 2*p.vx;
					p.ay = 2*p.vy;
					world.projectiles.add(p);
					reloading = 0;
					p.target = e;
					return;
				}
		reloading += 1/(double)reload;
	}
	public boolean canAttack(Enemy e)
	{
		return (e.position.x-position.x)*(e.position.x-position.x)+
			(e.position.y-position.y)*(e.position.y-position.y)<range*range;
	}
	public static double distance(java.awt.Point p, java.awt.Point p2)
	{
		return Math.sqrt((p2.x-p.x)*(p2.x-p.x)+(p2.y-p.y)*(p2.y-p.y));
	}
	public void setCost()
	{
		this.cost = (int)Math.sqrt((100*damage*Math.sqrt(range)*(specialness==3?5:1))/(reload));
	}
	public int getCorrectCost()
	{
		return (int)Math.sqrt((100*damage*Math.sqrt(range)*(specialness==3?5:1))/(reload));
	}
	public Tower clone()
	{
		Tower t = new Tower();
		t.damage = damage;
		t.size = size;
		t.color = color;
		t.shootType = shootType;
		t.reload = reload;
		t.range = range;
		t.reloading = reloading;
		t.specialness = specialness;
		t.position = position;
		t.world = world;
		t.cost = cost;
		return t;
	}
}