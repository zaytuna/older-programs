import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class GameDriver extends JFrame implements Runnable
{
	static Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

	World2D world = new World2D();
	DisplayPanel display = new DisplayPanel(world);
	SidePanel side;

	static int moneys = 100;
	static int wave = 0;
	static int health = 100;
	int monstersDeployed = 100;
	static double wait = 0;

	boolean kill = false;

	GameDriver()
	{
		side = new SidePanel(display,this);

		setUndecorated(true);
		setBounds(0,0,screen.width,screen.height);

		setLayout(null);

		display.setBounds(10,10,screen.width-20,screen.height-20);

		add(display);

		java.util.concurrent.ExecutorService service = java.util.concurrent.Executors.newCachedThreadPool();
		service.execute(this);
		service.shutdown();
	}
	public void run()
	{
		if(kill)try{Thread.sleep(1000);}catch(Exception e){}
		while(true)
		{
			try
			{
				Thread.sleep(30);
			}
			catch(Exception e){}

			if(health<=0)
			{
				JOptionPane.showMessageDialog(null,"You Loose");
				moneys = 100;
				wait = 0;
				monstersDeployed = 100;
				wave = 0;
				health = 100;
				world.clearAll();
			}

			if(kill || (Math.random()>.8 && (monstersDeployed<10*Math.log(wave+2))))
			{
				if(kill)
					wave+=30;
				monstersDeployed++;
				Enemy enemy = new Enemy();
				enemy.position = new Point(100+(int)(50*Math.cos(Math.random()*2*Math.PI)),
					100+(int)(50*Math.sin(Math.random()*2*Math.PI)));
				enemy.health = (wave+2)*3;
				enemy.maxHealth = (wave+2)*3;
				enemy.armor = 1;
				enemy.speed = 1+(Math.random()*2);
				enemy.to = new Point(screen.width,screen.height);
				world.enemies.add(enemy);
			}

			wait += .001;

			if(wait>=1)
			{
				wave++;
				monstersDeployed = 0;
				wait = 0;
			}

			world.update();
			side.repaint();
			display.repaint();
		}
	}
	public static void main(String[] args)
	{
		GameDriver driver = new GameDriver();
		driver.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		driver.setVisible(true);
	}
}